let express = require('express');
let router = express.Router();
let uuid = require('uuid/v4');
let Item = require("../db-schemas/user");
let util = require('util');
let months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];

router.post('/', function (req, res) {
    console.log(req.body)
    Item.findOne({email: req.body.po_email}).then(user => {

        console.log("user found")
        for (let i = 0; i < user.posts.length; i++) {
            if (user.posts[i].id === req.body.post_id) {
                let comment = {
                    comment_body: req.body.comment_body,
                    date: new Date().getDate() + " " + months[new Date().getMonth()],
                    postId: user.posts[i].id,
                    comment_owner_email: req.body.co_email,
                    comment_id: uuid(),
                    post_owner_email: user.email,
                    likes: [],
                    user: {}
                };
                //
                console.log('post found');
                comment.user.email = req.body.co_email;
                console.log("co found");
                user.posts[i].comments.push(comment);
                user.save();
                res.send({comment: comment})
            }
        }
    })
});

module.exports = router;