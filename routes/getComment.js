let express = require('express');
let router = express.Router();
let uuid = require('uuid/v4');
let Item = require("../db-schemas/user");
let util = require('util');
router.get('/', function (req, res) {
    console.log(req.body);
    console.log(req.body.email);
    console.log(req.body.post_id);
    let email = req.query.email;
    let post_id = req.query.post_id;
    let comment_id = req.query.comment_id;
    let result = {};
    Item.findOne({email: email}, function (err, user) {
        if (err) {
            res.send({err});
            console.log(err);
            return;
        }
        for (let i = 0; i < user.posts.length; i++) {
            console.log(user.posts[i].id);
            if (user.posts[i].id === post_id) {
                for (let j = 0; j < user.posts[i].comments.length; j++) {
                    if (user.posts[i].comments[j].comment_id === comment_id) {
                        console.log("comment found");

                        result.comment_body = user.posts[i].comments[j].comment_body;
                        result.date = user.posts[i].comments[j].date;
                        result.postId = user.posts[i].comments[j].postId;
                        result.comment_owner_email = user.posts[i].comments[j].comment_owner_email;
                        result.comment_id = user.posts[i].comments[j].comment_id;
                        result.post_owner_email = user.posts[i].comments[j].post_owner_email;
                        result.likes = user.posts[i].comments[j].likes;
                        Item.findOne({email: user.posts[i].comments[j].comment_owner_email}).then(comment_owner => {
                            console.log(comment_owner.id);
                            result.user = {
                                name: comment_owner.firstName + " " + comment_owner.lastName,
                                avatar: "",
                                verified: comment_owner.verified,
                                id: comment_owner.id
                            };
                            res.send({result: result})
                        })
                    }

                }
                console.log('finish')
            }
        }


    });
});
module.exports = router;
