let Item = require("../db-schemas/user");
let express = require('express');
let uuid = require('uuid/v4');
var router = express.Router();
var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;
var bcrypt = require('bcryptjs');

/* GET users listing. */
function validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
}

router.post('/', function (req, res) {
    const newUser = new Item({
        firstName: req.body.firstName,
        lastName: req.body.lastName,
        middleName: req.body.middleName,
        day_of_birth: req.body.day_of_birth,
        month_of_birth: req.body.month_of_birth,
        year_of_birth: req.body.year_of_birth,
        country: req.body.country,
        gender: req.body.gender,
        password: req.body.password,
        email: req.body.email,
        id: uuid()
    });

    if (validateEmail(newUser.email)) {
        Item.find({email: newUser.email}, function (err, email) {
            if (email.length > 0) {
                bcrypt.genSalt(10, function (err, salt) {
                    bcrypt.hash(newUser.password, salt, function (err, hash) {
                        newUser.password = hash;

                        Item.find({email: newUser.email}).then(() => res.json({exist: true, valid:true})).catch(err => res.json(err));
                    });
                });
                console.log("exist " + res)

            } else {
                bcrypt.genSalt(10, function (err, salt) {
                    bcrypt.hash(newUser.password, salt, function (err, hash) {
                        newUser.password = hash;
                        let user=newUser;
                        newUser.save().then(() => res.json({exist: false, valid:true, user})).catch(err => res.json(err));
                        console.log("added" + email)
                    });
                });

            }
        });

    }
    else {
        Item.find({email: newUser.email}).then(() => res.json({exist: true, valid:false})).catch(err => res.json(err));
    }
});
module.exports = router;
