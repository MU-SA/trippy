let express = require('express');
let router = express.Router();
let uuid = require('uuid/v4');
let Item = require("../db-schemas/user");
let util = require('util');
let months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];

router.post("/", function (req, res) {
    if (req.body) {
        console.log(req.body.data.images)
    }
    Item.findOne({email: req.body.data.email}, function (err, user) {
        let post = {
            body: req.body.data.post_content,
            postPictures: req.body.data.images,
            date: new Date().getDate() + " " + months[new Date().getMonth()],
            likes: [],
            comments: [],
            id: uuid()
        };

        user.posts.push(post);
        user.save();
        res.send({response: "done", post: post});
    })
});

function checkHasFile(formData) {
    var i = 0;
    for (var value of formData.values()) {
        i++;
    }
    return i;
}

function uploadImage(req, res) {
    console.log('file info: ', req.files.image);

    //split the url into an array and then get the last chunk and render it out in the send req.
    var pathArray = req.files.image.path.split('/');

    res.send(util.format(' Task Complete \n uploaded %s (%d Kb) to %s as %s'
        , req.files.image.name
        , req.files.image.size / 1024 | 0
        , req.files.image.path
        , req.body.title
        , req.files.image
        , '<img src="uploads/' + pathArray[(pathArray.length - 1)] + '">'
    ));
}

module.exports = router;
