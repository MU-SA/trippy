let Item = require("../db-schemas/user");
let express = require('express');
let bcrypt = require('bcryptjs');
let passport = require('passport');
const UserSession = require('../db-schemas/UserSessionSchema');
let router = express.Router();

/* GET users listing. */
function validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
}

router.get('/', (req, res) => {
    if (req.query.password && req.query.email) {
        const newItem = {
            password: req.query.password[0],
            email: req.query.email[0].toLowerCase()
        };
        if (validateEmail(newItem.email)) {
            Item.find({email: newItem.email}, function (err1, user) {

                if (user.length > 0) {

                    bcrypt.compare(newItem.password, user[0].password, function (err, isMatch) {

                        if (err) throw err;
                        console.log(isMatch);
                        if (isMatch) {
                            let id = user[0].id;
                            const userSession = new UserSession();
                            userSession.userId = user[0]._id;
                            userSession.save((err, doc) => {
                                if (err) {
                                    console.log(err);
                                }
                                console.log(user[0]);
                                Item.find({email: newItem.email})
                                    .then(() => res.json({exist: true, valid: true, user: user[0]}));
                                console.log(user[0]._id)

                            });
                        } else {
                            Item.find({email: newItem.email})
                                .then(() => res.json({exist: false, valid: true}));
                        }
                    });
                }
                else {
                    console.log("not");
                    Item.find({email: newItem.email})
                        .then(() => res.json({exist: false, valid: true}));
                }
            });

        }
        else {
            console.log(res);
            Item.find({email: newItem.email})
                .then(() => res.json({exist: true, valid: false}));
        }
    }
});

module.exports = router;
