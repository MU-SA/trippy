let express = require('express');
let router = express.Router();
let uuid = require('uuid/v4');
let Item = require("../db-schemas/user");
let util = require('util');
router.post('/', function (req, res) {
    console.log(req.body);
    Item.findOne({email: req.body.owner_email}).then(user => {
        if (user) {
            for (let i = 0; i < user.posts.length; i++) {
                if (user.posts[i].id === req.body.post_id) {
                    console.log("post exist");
                    for (let j = 0; j < user.posts[i].likes.length; j++) {
                        if (user.posts[i].likes[j].liked_by === req.body.liker_email) {
                            console.log("like exist");
                            user.posts[i].likes.splice(j, 1);
                            user.save();
                            console.log(user.posts[i].likes.length);
                            res.send({liked: false});
                            return
                        }
                    }
                    console.log("like not exist");

                    let like = {
                        liked_by: req.body.liker_email,
                        date: new Date().getDate() + "-" + new Date().getMonth() + 1 + "-" + new Date().getFullYear()
                    };
                    user.posts[i].likes.push(like);
                    user.save();
                    res.send({liked: true});
                    return;
                }
            }
        } else {
            console.log("not found");
            res.send({liked: false});
        }
    })
});

module.exports = router;