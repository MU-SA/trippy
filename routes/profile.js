let Item = require("../db-schemas/user");
let express = require('express');
let bcrypt = require('bcryptjs');
let passport = require('passport');
const UserSession = require('../db-schemas/UserSessionSchema');
let router = express.Router();
const User = require('../db-schemas/user');

router.get('/', (req, res) => {
    const newItem = {
        id: req.query.userId
    };
    console.log(newItem.id);

    Item.findOne({id: newItem.id}).then((user) => {
        console.log("user found")
        let result_user = {
            id: user.id,
            firstName: user.firstName,
            lastName: user.lastName,
            bio: user.bio,
            country: user.country,
            city: user.city,
            worksAt: user.worksAt,
            worksAs: user.worksAs,
            email: user.email,
            posts: user.posts
        };
        res.send({result_user});
    });
});
router.post('/', function (req, res) {

    User.update({email: req.body.data.email}, {
            id: req.body.data.id,
            firstName: req.body.data.firstName,
            lastName: req.body.data.lastName,
            bio: req.body.data.bio,
            country: req.body.data.country,
            city: req.body.data.city,
            worksAt: req.body.data.worksAt,
            worksAs: req.body.data.worksAs
        },
        function (err, ress) {
            if (err) {
                throw err
            }
            console.log(ress.nModified);
            if (ress.nModified > 0) {
                res.send('Saved');
                console.log(ress)
            } else {
                res.send('No changes')
            }
        });
});

module.exports = router;
