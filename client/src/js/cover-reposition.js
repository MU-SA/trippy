/* eslint-disable no-undef */
$(document).ready(function () {
    let img = $('.picture-container img');
    let y1 = $('.picture-container').height();
    let y2 = img.height();
    let x1 = $('.picture-container').width();
    let x2 = img.width();
    $('.save').click(function (event) {
        event.preventDefault();
        var t = img.position().top,
            l = img.position().left;
        img.attr('data-top', t);
        img.attr('data-left', l);
        img.draggable({disabled: true});
    });

    $('.pos').click(function (event) {
        event.preventDefault();
        img.draggable({
            disabled: false,
            scroll: false,
            axis: 'y, x',
            cursor: 'move',
            drag: function (event, ui) {
                if (ui.position.top >= 0) {
                    ui.position.top = 0;
                }
                if (ui.position.top <= y1 - y2) {
                    ui.position.top = y1 - y2;
                }
                if (ui.position.left >= 0) {
                    ui.position.left = 0;
                }

                if (ui.position.left <= x1 - x2) {
                    ui.position.left = x1 - x2;
                }
            }
        });
    });
});
