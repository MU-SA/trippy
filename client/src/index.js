import React from 'react';
import ReactDOM from 'react-dom';
import {BrowserRouter as Router, Route} from "react-router-dom";
import {BrowserRouter} from "react-router-dom";
import Main from './components/TrippyRouter';
import * as serviceWorker from './components/serviceWorker';
import Register from "./components/Register";
import ModalProfile from "./components/ProfileModal";
import Explore from "./components/Explore";
import Local from "./components/Local";
import Home from "./components/Home";
import Profile from "./components/Profile";
import Pg404 from "./components/404";
import Footer from "./components/Footer";
import TripsPage from "./components/TripsPage";
import EditBlog from "./components/EditBlog";

ReactDOM.render(
    <BrowserRouter forceRefresh={true}>
        <Router>
            <div>
                <Route exact path="" component={Main}/>
                <Route exact path="/explore" component={Explore}/>
                <Route exact path="/local" component={Local}/>
                <Route exact path="/login" component={ModalProfile}/>
                <Route exact path="/register" component={Register}/>
                <Route exact path="/home" component={Home}/>
                <Route path="/profile" onChange={onChange} component={Profile}/>
                <Route exact path="/404" component={Pg404}/>
                <Route exact path="/trips" component={TripsPage}/>
                <Route path="/edit_blog" component={EditBlog}/>
                <Route component={Footer}/>
            </div>
        </Router>

    </BrowserRouter>, document.getElementById('root'));
//
function onChange(e){
    alert(e.path)
}
// ReactDOM.render(<ProfilePic/>, document.getElementById('profile_pic'));
// ReactDOM.render(<Cover
//     src="https://images.unsplash.com/photo-1525577288853-c6f0a020a162?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=a76ff2c0280c0fcd31b5120ce3f11131&auto=format&fit=crop&w=1050&q=80"/>, document.getElementById('cover_page'));
// ReactDOM.render(<Info name='Muhammad Saleh' desc="Software Engineer and CEO at Trippy" location="San Francisco, California US"
//                       ocupy="Founder and CEO at Trippy"
//                       followersCount="15.8 M" followingCount="120" postsCount="25"/>, document.getElementById('info'));
// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA

serviceWorker.unregister();