import React, {Component} from "react";
import {Link} from 'react-router-dom';

class Place extends Component {
    render() {
        return (

            <div className="m-0 mb-2  p-0">
                <div className="card pr-0">
                    <div className="position-absolute right-0">
                        <i title={"Place"} className="far fa-home  p-2 backg-dark c-yellow"></i>
                        {this.props.ad === "yes" ?
                            <i title={"Ad"} className="far  fa-ad  p-2 bg-white c-dark"></i>
                            : null}


                    </div>
                    <div className="row m-2">

                        <img
                            src="https://images.pexels.com/photos/629142/pexels-photo-629142.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940"
                            style={{width: 50, height: 50}}
                            className="rounded-circle m-1 float-left"
                            alt="..."/>
                        <div className={"ml-1"}>
                            <p className="font-weight-bold c-blue" style={{marginTop: 10}}>
                                <Link to="/home" className="subtitle-1 c-dark">{this.props.name}</Link>
                                <span>
                                    <i className="fas ml-1 c-yellow fa-badge-check fa-sm">

                                    </i>
                                </span>
                            </p>
                            <p className="allcaps" style={{fontSize: 10, marginTop: -15}}>
                                {this.props.date}
                            </p>
                        </div>
                    </div>

                    <a href="#" title={this.props.name}
                       className="card border-0 ">
                        <img className="card-img-top rounded-0"
                             src={this.props.src}
                             alt="A Traveler’s Guide to Mid-Century Modern Architecture in New York City"/>
                    </a>

                    <p className=" pl-2  pr-2 pt-2  text-muted font-rubik reduce-font body-1">
                        {this.props.desc}
                    </p>
                    <div className="pl-2 pb-2 form-inline">
                        <button className="btn btn-primary">visit</button>
                    </div>
                </div>
            </div>

        );
    }
}

export default Place;