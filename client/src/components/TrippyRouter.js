import React from 'react';
import Welcome from './Welcome'
import Register from './Register'
import Home from './Home'
import {BrowserRouter as Router, Route, Switch} from "react-router-dom";
import {BrowserRouter} from 'react-router-dom';
import {getFromStorage} from '../storage/storage';

function Main() {
    /*user logging*/

    if (getFromStorage("main-app-storage")) {
        return (
            <BrowserRouter>
                <Router>
                    <Route path="/" component={Home}/>
                </Router>
            </BrowserRouter>
        )
    }
    return (
        <BrowserRouter>
            <Router>
                <Route path="/" component={Welcome}/>
            </Router>
        </BrowserRouter>
    )
}

export default Main;