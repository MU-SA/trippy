import React, {Component} from 'react';
import '../css/main.css';
import MasonryLayout from 'react-masonry-component';

import '../css/animate.min.css';

const masonryOptions = {
    transitionDuration: 0
};
const imagesLoadedOptions = {background: '.backg-dark'}

class Gallery extends Component {
    render() {
        return (
            <MasonryLayout
                className={''} // default ''
                elementType={'div'} // default 'div'
                options={masonryOptions} // default {}// default false
                updateOnEachImageLoad={true}
                percentPosition={true}// default false and works only if disableImagesLoaded is false
                imagesLoadedOptions={imagesLoadedOptions}>

                <div className="col-sm-12 col-md-6 col-lg-4 p-0">
                    <a href="https://images.unsplash.com/photo-1545776697-537826931936?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=60"
                       title="project name 2">
                        <div className="project_box_one">
                            <img
                                src="https://images.unsplash.com/photo-1545776697-537826931936?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=60"
                                alt="pro1"/>
                            <div className="product_info">
                                <div className="product_info_text">
                                    <div className="product_info_text_inner">
                                        <i className="ion ion-plus"></i>
                                        <h4>BALI JUN • 1 • 2018</h4>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>


                <div className="col-sm-12 col-md-6 col-lg-4 p-0 m-0">
                    <a href="https://images.unsplash.com/photo-1542838687-f960f044ef1b?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=334&q=80"
                       title="project name 1">
                        <div className="project_box_one">
                            <img
                                src="https://images.unsplash.com/photo-1542838687-f960f044ef1b?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=334&q=80"
                                alt="pro1"/>
                            <div className="product_info">
                                <div className="product_info_text">
                                    <div className="product_info_text_inner">
                                        <i className="ion ion-plus"></i>
                                        <h4>project name</h4>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>

                <div className="col-sm-12 col-md-6 col-lg-4 p-0">
                    <a href="https://images.unsplash.com/photo-1542838775-551ba96569a8?ixlib=rb-1.2.1&auto=format&fit=crop&w=500&q=60"
                       title="project name 2">
                        <div className="project_box_one">
                            <img
                                src="https://images.unsplash.com/photo-1542838775-551ba96569a8?ixlib=rb-1.2.1&auto=format&fit=crop&w=500&q=60"
                                alt="pro1"/>
                            <div className="product_info">
                                <div className="product_info_text">
                                    <div className="product_info_text_inner">
                                        <i className="ion ion-plus"></i>
                                        <h4>project name</h4>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>

                <div className="col-sm-12 col-md-6 col-lg-4 p-0">
                    <a href="https://images.unsplash.com/photo-1545647274-96644da34363?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1003&q=80"
                       title="project name 2">
                        <div className="project_box_one">
                            <img
                                src="https://images.unsplash.com/photo-1545647274-96644da34363?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1003&q=80"
                                alt="pro1"/>
                            <div className="product_info">
                                <div className="product_info_text">
                                    <div className="product_info_text_inner">
                                        <i className="ion ion-plus"></i>
                                        <h4>project name</h4>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                <div className="col-sm-12 col-md-6 col-lg-4 p-0">
                    <a href="https://images.unsplash.com/photo-1545727303-83cede79596a?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=60"
                       title="project name 2">
                        <div className="project_box_one">
                            <img
                                src="https://images.unsplash.com/photo-1545727303-83cede79596a?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=60"
                                alt="pro1"/>
                            <div className="product_info">
                                <div className="product_info_text">
                                    <div className="product_info_text_inner">
                                        <i className="ion ion-plus"></i>
                                        <h4>project name</h4>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>

                <div className="col-sm-12 col-md-6 col-lg-4 p-0">
                    <a href="https://images.unsplash.com/photo-1545775866-73f51c143381?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=60"
                       title="project name 2">
                        <div className="project_box_one">
                            <img
                                src="https://images.unsplash.com/photo-1545775866-73f51c143381?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=60"
                                alt="pro1"/>
                            <div className="product_info">
                                <div className="product_info_text">
                                    <div className="product_info_text_inner">
                                        <i className="ion ion-plus"></i>
                                        <h4>project name</h4>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>

                <div className="col-sm-12 col-md-4 col-lg-4 p-0">
                    <a href="https://images.unsplash.com/photo-1545772274-f270b6552fa4?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=60"
                       title="project name 2">
                        <div className="project_box_one">
                            <img
                                src="https://images.unsplash.com/photo-1545772274-f270b6552fa4?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=60"
                                alt="pro1"/>
                            <div className="product_info">
                                <div className="product_info_text">
                                    <div className="product_info_text_inner">
                                        <i className="ion ion-plus"></i>
                                        <h4>project name</h4>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>

                <div className="col-sm-12 col-md-4 col-lg-4 p-0">
                    <a href="https://images.unsplash.com/photo-1545748806-5ce42a3644a3?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=60"
                       title="project name 2">
                        <div className="project_box_one">
                            <img
                                src="https://images.unsplash.com/photo-1545748806-5ce42a3644a3?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=60"
                                alt="pro1"/>
                            <div className="product_info">
                                <div className="product_info_text">
                                    <div className="product_info_text_inner">
                                        <i className="ion ion-plus"></i>
                                        <h4>project name</h4>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>

                <div className="col-sm-12 col-md-4 col-lg-4 p-0">
                    <a href="https://images.unsplash.com/photo-1545705754-c312e4836414?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=60"
                       title="project name 2">
                        <div className="project_box_one">
                            <img
                                src="https://images.unsplash.com/photo-1545705754-c312e4836414?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=60"
                                alt="pro1"/>
                            <div className="product_info">
                                <div className="product_info_text">
                                    <div className="product_info_text_inner">
                                        <i className="ion ion-plus"></i>
                                        <h4>project name</h4>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                <div className="col-sm-12 col-md-4 col-lg-4 p-0">
                    <a href="../imgs/favicon.png"
                       title="project name 2">
                        <div className="project_box_one">
                            <img
                                src="http://musa.rf.gd/images/about-2.png"
                                alt="pro1"/>
                            <div className="product_info">
                                <div className="product_info_text">
                                    <div className="product_info_text_inner">
                                        <i className="ion ion-plus"></i>
                                        <h4>project name</h4>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>


            </MasonryLayout>

        )
    }
}
export default Gallery;