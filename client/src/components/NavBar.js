import {getFromStorage} from "../storage/storage";
import React, {Component} from 'react';
import {Link} from 'react-router-dom';
import src from '../imgs/IMG_20190105_144134070.jpg'
// eslint-disable-next-line
let inst = this;

class NavBar extends Component {
    getuserId = () => {
        return (getFromStorage("main-app-storage").id);
    };

    constructor(props) {
        super(props);
        // this.props.history = this.props.prop;
        inst = this.props.instance;
    }

    render() {
        return (
            <nav className="navbar navbar-expand-lg box-shadow bg-white">
                <button className="navbar-toggler" type="button" data-toggle="collapse"
                        data-target="#navbarSupportedContent"
                        aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                </button>
                <div className="collapse container navbar-collapse nav-res" id="navbarSupportedContent">
                    <Link className="navbar-brand m-0" to={"/home"} onClick={() => {
                        this.props.history.push("/home")
                    }} data-toggle="modal" data-target="#exampleModalCenter">

                        <div className="small-rect unselectable backg-yellow subtitle-1 font-weight-bold m-0 c-dark"
                             style={{fontSize: 12}}>TRIP
                        </div>
                    </Link>
                    <input className="form-control align-self-center form-control-sm c-dark"
                           style={{margin: 'auto', fontSize: 12, maxWidth: 180, fontFamily: 'Rubik,serif'}}
                           type="text"
                           placeholder="Search"/>
                    <Link to={"/profile/" + this.getuserId()} className="mr-3">
                        <i className="fal fa-user-alt c-blue fa-lg art-a">
                        </i>
                    </Link>
                    <a>
                        <i data-container="body"
                           data-toggle="popover"
                           data-placement="bottom"
                           className="fal c-blue fa-globe-americas fa-lg art-a">
                                <span className="artificial-badge">
                                </span>
                        </i>
                    </a>
                    <div id="popover-content" style={{display: "none"}}>
                        <ul className="list-group mt-0 text-left">
                            <li className="row ml-0 cursor_pointer mr-0 mb-0 mt-0 p-0">
                                <div className="col-sm-1 m-0 p-0">
                                    <span style={{width: "2px", height: "2px", marginTop: "50%"}}
                                          className='backg-pink p-1 rounded-circle position-absolute'>

                                    </span>
                                    <img className="ml-3 mb-0" src={src} height={30} width={30}/>
                                </div>
                                <div className="col-sm p-0 m-0  text-left pl-4">
                                    <p className="caption ml-2 p-0 m-0"> Muhammad Saleh Started follwing </p>
                                </div>
                            </li>

                            <div style={{minHeight: 1, minWidth: "100%", marginTop: "10px", marginBottom: "10px"}}
                                 className={"back-dark-gray"}>

                            </div><li className="row ml-0 cursor_pointer mr-0 mb-0 mt-0 p-0">
                                <div className="col-sm-1 m-0 p-0">
                                    <span style={{width: "2px", height: "2px", marginTop: "50%"}}
                                          className='backg-pink p-1 rounded-circle position-absolute'>

                                    </span>
                                    <img className="ml-3 mb-0" src={src} height={30} width={30}/>
                                </div>
                                <div className="col-sm p-0 m-0  text-left pl-4">
                                    <p className="caption ml-2 p-0 m-0"> Muhammad Saleh Started follwing </p>
                                </div>
                            </li>

                            <div style={{minHeight: 1, minWidth: "100%", marginTop: "10px", marginBottom: "10px"}}
                                 className={"back-dark-gray"}>

                            </div><li className="row ml-0 cursor_pointer mr-0 mb-0 mt-0 p-0">
                                <div className="col-sm-1 m-0 p-0">
                                    <span style={{width: "2px", height: "2px", marginTop: "50%"}}
                                          className='backg-pink p-1 rounded-circle position-absolute'>

                                    </span>
                                    <img className="ml-3 mb-0" src={src} height={30} width={30}/>
                                </div>
                                <div className="col-sm p-0 m-0  text-left pl-4">
                                    <p className="caption ml-2 p-0 m-0"> Muhammad Saleh Started follwing </p>
                                </div>
                            </li>

                            <div style={{minHeight: 1, minWidth: "100%", marginTop: "10px", marginBottom: "10px"}}
                                 className={"back-dark-gray"}>

                            </div><li className="row ml-0 cursor_pointer mr-0 mb-0 mt-0 p-0">
                                <div className="col-sm-1 m-0 p-0">
                                    <span style={{width: "2px", height: "2px", marginTop: "50%"}}
                                          className='backg-pink p-1 rounded-circle position-absolute'>

                                    </span>
                                    <img className="ml-3 mb-0" src={src} height={30} width={30}/>
                                </div>
                                <div className="col-sm p-0 m-0  text-left pl-4">
                                    <p className="caption ml-2 p-0 m-0"> Muhammad Saleh Started follwing </p>
                                </div>
                            </li>

                            <div style={{minHeight: 1, minWidth: "100%", marginTop: "10px", marginBottom: "10px"}}
                                 className={"back-dark-gray"}>

                            </div>
                            <a className="c-blue caption text-center">See more</a>
                        </ul>
                    </div>
                </div>
            </nav>
        )
    }
}

export default NavBar;