import React, {Component} from 'react';
import '../register/css/style.css';
import '../css/bootstrap.css';
import '../css/fa-pro-5.0/fontawesome-pro-5.3.1-web/css/all.css';
import {getFromStorage, setInStorage} from "../storage/storage";
import {Link} from "react-router-dom";
import axios from "axios";

let message = "";

function Errors() {
    return (

        <div className={"p-2 alert border-0 card backg-dark right-0"}>
            <div className="row c-white" role="alert">
                <div className={"col-sm-1"} style={{marginTop: "3px"}}>
                    <i className="c-pink fal fa-exclamation-circle"></i>
                </div>

                <div>
                    <div>
                        <strong className={"c-white"}>{message.header}</strong>
                    </div>
                    <small className={"text-muted"}>{message.message}</small>
                </div>
            </div>


        </div>

    );
}

function Exist() {
    return (<div className={"p-2 alert border-0 card backg-dark right-0"}>
            <div className="row c-white" role="alert">
                <div className={"col-sm-1"} style={{marginTop: "3px"}}>
                    <i className="fal fa-registered c-pink"></i>
                </div>

                <div>
                    <div>
                        <strong className={"c-white"}>Already exists</strong>
                    </div>
                    <Link to={'/login'} onClick={() => {
                        this.props.history.push('/login')
                    }}>
                        <small className={"text-muted"}>Go to the login page</small>
                    </Link>
                </div>
            </div>
        </div>
    )
}

function Loading() {
    return (
        <div className={"text-center mb-2"}>
            <svg version="1.1" id="loader-1" xmlns="http://www.w3.org/2000/svg"
                 xmlnsXlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                 width="40px" height="40px" viewBox="0 0 40 40" enableBackground="new 0 0 40 40" xmlSpace="preserve">
                <path opacity="0.2" fill="#000" d="M20.201,5.169c-8.254,0-14.946,6.692-14.946,14.946c0,8.255,6.692,14.946,14.946,14.946
    s14.946-6.691,14.946-14.946C35.146,11.861,28.455,5.169,20.201,5.169z M20.201,31.749c-6.425,0-11.634-5.208-11.634-11.634
    c0-6.425,5.209-11.634,11.634-11.634c6.425,0,11.633,5.209,11.633,11.634C31.834,26.541,26.626,31.749,20.201,31.749z"/>
                <path fill="#000" d="M26.013,10.047l1.654-2.866c-2.198-1.272-4.743-2.012-7.466-2.012h0v3.312h0
    C22.32,8.481,24.301,9.057,26.013,10.047z">
                    <animateTransform attributeType="xml"
                                      attributeName="transform"
                                      type="rotate"
                                      from="0 20 20"
                                      to="360 20 20"
                                      dur="0.5s"
                                      repeatCount="indefinite"/>
                </path>
            </svg>
        </div>
    )
}

class Register extends Component {

    componentWillMount() {
        document.title = 'Trippy | Register';
        if(getFromStorage("main-app-storage")){
            this.props.history.push("");
        }
    };

    val = text => {
        if (this.state[text] === "") {
            message = {
                header: "Missing data",
                message: "Make sure you filled the required fields"
            };
            this.setState({validate: false});
            this.setState({loading: false});
            this.state.validate = false;

        }
    };

    validate = () => {
        Object.keys(this.state).map(this.val)
    };

    register = (e) => {
        e.preventDefault();
        this.setState({loading: true});
        this.validate();
        if (this.state.validate) {
            this.setState({validate: true});
            axios.post('/register', this.state)
                .then(response => {
                    if (response.data.valid === false) {
                        message = {
                            header: "Email is not valid",
                            message: "Enter a valid email address"
                        };
                        this.setState({validate: false});
                        this.setState({exist: false})
                    } else if (response.data.valid === true) {
                        if (response.data.exist === false) {
                            console.log(getFromStorage("main-app-storage"));
                            console.log(response);
                            setInStorage("main-app-storage", response.data.user);
                            this.props.history.push('/home')
                        } else if (response.data.exist === true) {
                            this.setState({exist: true})
                        }
                    }
                    this.setState({loading: false})

                })
                .catch(error => {
                    console.log(error + "as");
                    this.setState({loading: false});
                    this.setState({exist: true});

                });
        }

    };


    onChange(e) {
        this.state.validate = true;
        this.state.exist = false;
        if (e.target.name === "accept") {
            this.setState({accept: e.target.checked})
        }
        else if (e.target.name === "confirmed_pass") {
            this.setState({[e.target.name]: e.target.value});
            if (this.state.password === e.target.value) {
                this.setState({confirmed: true})
            } else {
                this.setState({confirmed: false})
            }
        }


        else if (e.target.name === "password") {
            if (e.target.value) {
                this.setState({[e.target.name]: e.target.value});
                if (this.state.confirmed_pass === e.target.value) {
                    this.setState({confirmed: true})
                } else {
                    this.setState({confirmed: false})
                }
            }
        }
        else {
            this.setState({[e.target.name]: e.target.value});
        }
    }

    constructor(props) {
        super(props);
        this.state = {
            firstName: "",
            middleName: "",
            lastName: "",
            validate: true,
            exist: false,
            gender: "",
            day_of_birth: "",
            month_of_birth: "",
            year_of_birth: "",
            email: "",
            country: "",
            password: "",
            confirmed_pass: "",
            confirmed: false,
            loading: false,
            accept: false
        };

        this.register = this.register.bind(this);
        this.onChange = this.onChange.bind(this);
    }


    render() {
        return (
            <section>
                <Link to={''} className={"c-dark"} onClick={() => {
                    this.props.history.push('')
                }}>
                    <div className="rect fixed-top font-weight-bold nav-res" style={{fontSize: 24}}>TRIP</div>
                </Link>
                <div className="register-container box-shadow bg-white">
                    <form>
                        <div className="row">
                            <h5>Account</h5>
                            <div className="row">
                                <div className="input-group  col-sm">
                                    <input onChange={this.onChange} name={"firstName"} type="text"
                                           placeholder="First Name"/>
                                </div>
                                <div className="input-group col-sm">
                                    <input onChange={this.onChange} name={"middleName"} type="text"
                                           placeholder="Middel Name"/>
                                </div>
                                <div className="input-group col-sm">
                                    <input onChange={this.onChange} name={"lastName"} type="text"
                                           placeholder="Last Name"/>
                                </div>

                            </div>

                            <div className="input-group">
                                <input onChange={this.onChange} name={"email"} type="email"
                                       placeholder="Email Address"/>
                            </div>
                            <div className="input-group">
                                <input onChange={this.onChange} name={"password"} type="password"
                                       placeholder="Password"/>
                            </div>
                            <div className="input-group ">
                                <input onChange={this.onChange} name={"confirmed_pass"} type="password"
                                       placeholder="Confirm Password"/>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-half">
                                <h5>Date of Birth</h5>
                                <div className="input-group">
                                    <div className="col-third">
                                        <input onChange={this.onChange} name={"day_of_birth"} type="number"
                                               placeholder="DD"/>
                                    </div>
                                    <div className="col-third">
                                        <input onChange={this.onChange} name={"month_of_birth"} type="number"
                                               placeholder="MM"/>
                                    </div>
                                    <div className="col-third">
                                        <input onChange={this.onChange} name={"year_of_birth"} type="number"
                                               placeholder="YYYY"/>
                                    </div>
                                </div>
                            </div>
                            <div className="col-half">
                                <h5>Gender</h5>
                                <div className="input-group">
                                    <input onChange={this.onChange} type="radio" name="gender" value="male"
                                           id="gender-male"/>
                                    <label htmlFor="gender-male">Male</label>
                                    <input onChange={this.onChange} type="radio" name="gender" value="female"
                                           id="gender-female"/>
                                    <label htmlFor="gender-female">Female</label>
                                </div>

                            </div>
                            <div className="input-group mr-3">
                                <select onChange={this.onChange} name={"country"}>
                                    <option value="">Country</option>
                                    <option value="Afghanistan">Afghanistan</option>
                                    <option value="Albania">Albania</option>
                                    <option value="Algeria">Algeria</option>
                                    <option value="American Samoa">American Samoa</option>
                                    <option value="Andorra">Andorra</option>
                                    <option value="Angola">Angola</option>
                                    <option value="Anguilla">Anguilla</option>
                                    <option value="Antigua & Barbuda">Antigua &amp; Barbuda</option>
                                    <option value="Argentina">Argentina</option>
                                    <option value="Armenia">Armenia</option>
                                    <option value="Aruba">Aruba</option>
                                    <option value="Australia">Australia</option>
                                    <option value="Austria">Austria</option>
                                    <option value="Azerbaijan">Azerbaijan</option>
                                    <option value="Bahamas">Bahamas</option>
                                    <option value="Bahrain">Bahrain</option>
                                    <option value="Bangladesh">Bangladesh</option>
                                    <option value="Barbados">Barbados</option>
                                    <option value="Belarus">Belarus</option>
                                    <option value="Belgium">Belgium</option>
                                    <option value="Belize">Belize</option>
                                    <option value="Benin">Benin</option>
                                    <option value="Bermuda">Bermuda</option>
                                    <option value="Bhutan">Bhutan</option>
                                    <option value="Bolivia">Bolivia</option>
                                    <option value="Bonaire">Bonaire</option>
                                    <option value="Bosnia & Herzegovina">Bosnia &amp; Herzegovina</option>
                                    <option value="Botswana">Botswana</option>
                                    <option value="Brazil">Brazil</option>
                                    <option value="British Indian Ocean Ter">British Indian Ocean Ter</option>
                                    <option value="Brunei">Brunei</option>
                                    <option value="Bulgaria">Bulgaria</option>
                                    <option value="Burkina Faso">Burkina Faso</option>
                                    <option value="Burundi">Burundi</option>
                                    <option value="Cambodia">Cambodia</option>
                                    <option value="Cameroon">Cameroon</option>
                                    <option value="Canada">Canada</option>
                                    <option value="Canary Islands">Canary Islands</option>
                                    <option value="Cape Verde">Cape Verde</option>
                                    <option value="Cayman Islands">Cayman Islands</option>
                                    <option value="Central African Republic">Central African Republic</option>
                                    <option value="Chad">Chad</option>
                                    <option value="CD">Channel Islands</option>
                                    <option value="CL">Chile</option>
                                    <option value="CN">China</option>
                                    <option value="CI">Christmas Island</option>
                                    <option value="CS">Cocos Island</option>
                                    <option value="CO">Colombia</option>
                                    <option value="CC">Comoros</option>
                                    <option value="CG">Congo</option>
                                    <option value="CK">Cook Islands</option>
                                    <option value="CR">Costa Rica</option>
                                    <option value="CT">Cote D'Ivoire</option>
                                    <option value="HR">Croatia</option>
                                    <option value="CU">Cuba</option>
                                    <option value="CB">Curacao</option>
                                    <option value="CY">Cyprus</option>
                                    <option value="CZ">Czech Republic</option>
                                    <option value="DK">Denmark</option>
                                    <option value="DJ">Djibouti</option>
                                    <option value="DM">Dominica</option>
                                    <option value="DO">Dominican Republic</option>
                                    <option value="TM">East Timor</option>
                                    <option value="EC">Ecuador</option>
                                    <option value="EG">Egypt</option>
                                    <option value="SV">El Salvador</option>
                                    <option value="GQ">Equatorial Guinea</option>
                                    <option value="ER">Eritrea</option>
                                    <option value="EE">Estonia</option>
                                    <option value="ET">Ethiopia</option>
                                    <option value="FA">Falkland Islands</option>
                                    <option value="FO">Faroe Islands</option>
                                    <option value="FJ">Fiji</option>
                                    <option value="FI">Finland</option>
                                    <option value="FR">France</option>
                                    <option value="GF">French Guiana</option>
                                    <option value="PF">French Polynesia</option>
                                    <option value="FS">French Southern Ter</option>
                                    <option value="GA">Gabon</option>
                                    <option value="GM">Gambia</option>
                                    <option value="GE">Georgia</option>
                                    <option value="DE">Germany</option>
                                    <option value="GH">Ghana</option>
                                    <option value="GI">Gibraltar</option>
                                    <option value="GB">Great Britain</option>
                                    <option value="GR">Greece</option>
                                    <option value="GL">Greenland</option>
                                    <option value="GD">Grenada</option>
                                    <option value="GP">Guadeloupe</option>
                                    <option value="GU">Guam</option>
                                    <option value="GT">Guatemala</option>
                                    <option value="GN">Guinea</option>
                                    <option value="GY">Guyana</option>
                                    <option value="HT">Haiti</option>
                                    <option value="HW">Hawaii</option>
                                    <option value="HN">Honduras</option>
                                    <option value="HK">Hong Kong</option>
                                    <option value="HU">Hungary</option>
                                    <option value="IS">Iceland</option>
                                    <option value="IN">India</option>
                                    <option value="ID">Indonesia</option>
                                    <option value="IA">Iran</option>
                                    <option value="IQ">Iraq</option>
                                    <option value="IR">Ireland</option>
                                    <option value="IM">Isle of Man</option>
                                    <option value="IL">Israel</option>
                                    <option value="IT">Italy</option>
                                    <option value="JM">Jamaica</option>
                                    <option value="JP">Japan</option>
                                    <option value="JO">Jordan</option>
                                    <option value="KZ">Kazakhstan</option>
                                    <option value="KE">Kenya</option>
                                    <option value="KI">Kiribati</option>
                                    <option value="NK">Korea North</option>
                                    <option value="KS">Korea South</option>
                                    <option value="KW">Kuwait</option>
                                    <option value="KG">Kyrgyzstan</option>
                                    <option value="LA">Laos</option>
                                    <option value="LV">Latvia</option>
                                    <option value="LB">Lebanon</option>
                                    <option value="LS">Lesotho</option>
                                    <option value="LR">Liberia</option>
                                    <option value="LY">Libya</option>
                                    <option value="LI">Liechtenstein</option>
                                    <option value="LT">Lithuania</option>
                                    <option value="LU">Luxembourg</option>
                                    <option value="MO">Macau</option>
                                    <option value="MK">Macedonia</option>
                                    <option value="MG">Madagascar</option>
                                    <option value="MY">Malaysia</option>
                                    <option value="MW">Malawi</option>
                                    <option value="MV">Maldives</option>
                                    <option value="ML">Mali</option>
                                    <option value="MT">Malta</option>
                                    <option value="MH">Marshall Islands</option>
                                    <option value="MQ">Martinique</option>
                                    <option value="MR">Mauritania</option>
                                    <option value="MU">Mauritius</option>
                                    <option value="ME">Mayotte</option>
                                    <option value="MX">Mexico</option>
                                    <option value="MI">Midway Islands</option>
                                    <option value="MD">Moldova</option>
                                    <option value="MC">Monaco</option>
                                    <option value="MN">Mongolia</option>
                                    <option value="MS">Montserrat</option>
                                    <option value="MA">Morocco</option>
                                    <option value="MZ">Mozambique</option>
                                    <option value="MM">Myanmar</option>
                                    <option value="NA">Nambia</option>
                                    <option value="NU">Nauru</option>
                                    <option value="NP">Nepal</option>
                                    <option value="AN">Netherland Antilles</option>
                                    <option value="NL">Netherlands (Holland, Europe)</option>
                                    <option value="NV">Nevis</option>
                                    <option value="NC">New Caledonia</option>
                                    <option value="NZ">New Zealand</option>
                                    <option value="NI">Nicaragua</option>
                                    <option value="NE">Niger</option>
                                    <option value="NG">Nigeria</option>
                                    <option value="NW">Niue</option>
                                    <option value="NF">Norfolk Island</option>
                                    <option value="NO">Norway</option>
                                    <option value="OM">Oman</option>
                                    <option value="PK">Pakistan</option>
                                    <option value="PW">Palau Island</option>
                                    <option value="PS">Palestine</option>
                                    <option value="PA">Panama</option>
                                    <option value="PG">Papua New Guinea</option>
                                    <option value="PY">Paraguay</option>
                                    <option value="PE">Peru</option>
                                    <option value="PH">Philippines</option>
                                    <option value="PO">Pitcairn Island</option>
                                    <option value="PL">Poland</option>
                                    <option value="PT">Portugal</option>
                                    <option value="PR">Puerto Rico</option>
                                    <option value="QA">Qatar</option>
                                    <option value="ME">Republic of Montenegro</option>
                                    <option value="RS">Republic of Serbia</option>
                                    <option value="RE">Reunion</option>
                                    <option value="RO">Romania</option>
                                    <option value="RU">Russia</option>
                                    <option value="RW">Rwanda</option>
                                    <option value="NT">St Barthelemy</option>
                                    <option value="EU">St Eustatius</option>
                                    <option value="HE">St Helena</option>
                                    <option value="KN">St Kitts-Nevis</option>
                                    <option value="LC">St Lucia</option>
                                    <option value="MB">St Maarten</option>
                                    <option value="PM">St Pierre &amp; Miquelon</option>
                                    <option value="VC">St Vincent &amp; Grenadines</option>
                                    <option value="SP">Saipan</option>
                                    <option value="SO">Samoa</option>
                                    <option value="AS">Samoa American</option>
                                    <option value="SM">San Marino</option>
                                    <option value="ST">Sao Tome &amp; Principe</option>
                                    <option value="SA">Saudi Arabia</option>
                                    <option value="SN">Senegal</option>
                                    <option value="RS">Serbia</option>
                                    <option value="SC">Seychelles</option>
                                    <option value="SL">Sierra Leone</option>
                                    <option value="SG">Singapore</option>
                                    <option value="SK">Slovakia</option>
                                    <option value="SI">Slovenia</option>
                                    <option value="SB">Solomon Islands</option>
                                    <option value="OI">Somalia</option>
                                    <option value="ZA">South Africa</option>
                                    <option value="ES">Spain</option>
                                    <option value="LK">Sri Lanka</option>
                                    <option value="SD">Sudan</option>
                                    <option value="SR">Suriname</option>
                                    <option value="SZ">Swaziland</option>
                                    <option value="SE">Sweden</option>
                                    <option value="CH">Switzerland</option>
                                    <option value="SY">Syria</option>
                                    <option value="TA">Tahiti</option>
                                    <option value="TW">Taiwan</option>
                                    <option value="TJ">Tajikistan</option>
                                    <option value="TZ">Tanzania</option>
                                    <option value="TH">Thailand</option>
                                    <option value="TG">Togo</option>
                                    <option value="TK">Tokelau</option>
                                    <option value="TO">Tonga</option>
                                    <option value="TT">Trinidad &amp; Tobago</option>
                                    <option value="TN">Tunisia</option>
                                    <option value="TR">Turkey</option>
                                    <option value="TU">Turkmenistan</option>
                                    <option value="TC">Turks &amp; Caicos Is</option>
                                    <option value="TV">Tuvalu</option>
                                    <option value="UG">Uganda</option>
                                    <option value="UA">Ukraine</option>
                                    <option value="AE">United Arab Emirates</option>
                                    <option value="GB">United Kingdom</option>
                                    <option value="US">United States of America</option>
                                    <option value="UY">Uruguay</option>
                                    <option value="UZ">Uzbekistan</option>
                                    <option value="VU">Vanuatu</option>
                                    <option value="VS">Vatican City State</option>
                                    <option value="VE">Venezuela</option>
                                    <option value="VN">Vietnam</option>
                                    <option value="VB">Virgin Islands (Brit)</option>
                                    <option value="VA">Virgin Islands (USA)</option>
                                    <option value="WK">Wake Island</option>
                                    <option value="WF">Wallis &amp; Futana Is</option>
                                    <option value="YE">Yemen</option>
                                    <option value="ZR">Zaire</option>
                                    <option value="ZM">Zambia</option>
                                    <option value="ZW">Zimbabwe</option>
                                </select>
                            </div>

                        </div>
                        <div className="row">
                            <h4>Terms and Conditions</h4>
                            <div className="input-group">
                                <input onChange={this.onChange} name={"accept"} type="checkbox" id="terms"/>
                                <label htmlFor="terms" className={"body-2"}>I accept the terms and conditions for
                                    signing up to this
                                    service.</label>
                            </div>

                        </div>
                        {this.state.validate ? null : <Errors/>}
                        {this.state.exist ? <Exist/> : null}
                        {this.state.loading ? <Loading/> : null}

                        <button disabled={!this.state.accept || !this.state.confirmed || this.state.loading}
                                className="r-btn c-white button-text backg-blue border-0"
                                onClick={this.register}
                                style={{height: 60}}>Sign up
                        </button>
                    </form>
                </div>

            </section>
        )
    }
}

export default Register;