import React, {Component} from 'react';
import '../css/login.css';
import '../css/bootstrap.css';
import '../css/shimmer.css';
import {Route, Redirect} from 'react-router'
import "../css/fa-pro-5.0/fontawesome-pro-5.3.1-web/css/all.css";
import src from '../imgs/favicon.png';
import axios from 'axios';
import {getFromStorage, setInStorage} from "../storage/storage";

function Loading() {
    return (
        <div className={"text-center mb-2"}>
            <svg version="1.1" id="loader-1" xmlns="http://www.w3.org/2000/svg"
                 xmlnsXlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                 width="40px" height="40px" viewBox="0 0 40 40" enableBackground="new 0 0 40 40" xmlSpace="preserve">
                <path opacity="0.2" fill="#000" d="M20.201,5.169c-8.254,0-14.946,6.692-14.946,14.946c0,8.255,6.692,14.946,14.946,14.946
    s14.946-6.691,14.946-14.946C35.146,11.861,28.455,5.169,20.201,5.169z M20.201,31.749c-6.425,0-11.634-5.208-11.634-11.634
    c0-6.425,5.209-11.634,11.634-11.634c6.425,0,11.633,5.209,11.633,11.634C31.834,26.541,26.626,31.749,20.201,31.749z"/>
                <path fill="#000" d="M26.013,10.047l1.654-2.866c-2.198-1.272-4.743-2.012-7.466-2.012h0v3.312h0
    C22.32,8.481,24.301,9.057,26.013,10.047z">
                    <animateTransform attributeType="xml"
                                      attributeName="transform"
                                      type="rotate"
                                      from="0 20 20"
                                      to="360 20 20"
                                      dur="0.5s"
                                      repeatCount="indefinite"/>
                </path>
            </svg>
        </div>
    )
}


class UpdateAbout extends Component {

    save = (e) => {
        e.preventDefault();
        this.setState({bio: this.refs.bio.innerText});
        this.setState({loading: true});
        axios.post('/profile', {id: '' + this.props.data.id, data: this.state}).then(response => {
            this.refs.saved.innerHTML = response.data;
            if (response.data === "Saved") {
                setInStorage("main-app-storage", this.state);
                window.location.reload();

            }
            this.setState({loading: false});
        });
    };


    onChange = (e) => {
        this.refs.saved.innerHTML = 'Save';
        this.setState({
            [e.target.name]: e.target.value
        });
    };

    goHome = () => {
        this.props.history.push("/home");
    };

    constructor(props) {
        super(props);

        this.state = {
            firstName: this.props.data.firstName,
            loading: false,
            lastName: this.props.data.lastName,
            bio: this.props.data.bio,
            worksAs: this.props.data.worksAs,
            worksAt: this.props.data.worksAt,
            email: this.props.data.email,
            id: this.props.data.id,
            country: this.props.data.country,
            city: this.props.data.city
        };

        this.goHome = this.goHome.bind(this);

    }

    render() {
        return (
            <div className="col-lg-9 bg-light">
                {!this.state.firstName ? <Shimmer/> :
                    <div className="container">
                        <div style={{marginRight: "-15px", marginLeft: "-15px",}}
                             className="p-3 rounded-top backg-dark text-left">
                            <div className={"c-white subtitle-1"}>Update info</div>
                            <p className="body-2 m-0 text-muted">Be specific when updating your info to let people
                                reach
                                you
                                easily</p>
                        </div>
                        <div className="p-3 row">
                            <div className="col-md-4 order-md-2 mb-4">
                                <h4 className="d-flex justify-content-between align-items-center mb-3">
                                    <div className="body-2 c-dark">Your Places</div>
                                    <span className="badge badge-secondary badge-pill">0</span>
                                </h4>
                                <ul className="list-group p-0 mb-5">

                                    <li className="d-flex justify-content-between lh-condensed p-0">
                                        <div className={"row align-items-center p-0 m-0"}>
                                            <img src={src} className={"mt-2 mb-2"} height={"50"} width={"50"}/>
                                            <div className={"text-left pl-2"}>
                                                <div className="my-0 subtitle-1 c-blue p-0">Cafe
                                                    <i className="fas c-yellow fa-badge-check fa-sm"> </i></div>
                                                <small className="allcaps c-blue">210K Likes</small>
                                            </div>
                                        </div>

                                    </li>
                                    <li className="d-flex justify-content-between lh-condensed p-0">
                                        <div className={"row align-items-center p-0 m-0"}>
                                            <img src={src} className={"mt-2 mb-2"} height={"50"} width={"50"}/>
                                            <div className={"text-left pl-2"}>
                                                <div className="my-0 subtitle-1 c-blue p-0">Cafe
                                                    <i className="ml-1 fas c-yellow fa-badge-check fa-sm"> </i>
                                                </div>
                                                <small className="allcaps c-blue">210K Likes</small>
                                            </div>
                                        </div>

                                    </li>


                                </ul>


                                <h4 className="d-flex justify-content-between  align-items-center mb-3">
                                    <div className="body-2 c-dark">Your Following</div>
                                    <span className="badge badge-secondary badge-pill">210</span>
                                </h4>


                                <form className="card border-0">
                                    <div className="input-group">
                                        <input type="text" className="form-control caption" placeholder="Name"/>
                                        <div className="input-group-append">
                                            <button type="submit" className="btn btn-secondary">Search</button>
                                        </div>
                                    </div>
                                </form>


                                <ul className="list-group border-top p-0 mb-3">
                                    <li className="border border-top-0 d-flex justify-content-between lh-condensed p-0">
                                        <div className={"row align-items-center p-0 m-0"}>
                                            <img src={src} className={"mt-2 mb-2 ml-2 rounded-circle"} height={"50"}
                                                 width={"50"}/>
                                            <div className={"text-left pl-2"}>
                                                <div className={"form-inline"}>
                                                    <a href={"#"}>
                                                        <div className="my-0 subtitle-1 c-blue p-0">Bella Yassin
                                                            <i className="fas c-yellow ml-1 fa-badge-check fa-sm"> </i>
                                                        </div>
                                                    </a>

                                                    <a href={"#"} className={'ml-5'}>
                                                        <i className={"fas fa-user-minus"}> </i>
                                                        <i className={"fal fa-user-plus display-none"}> </i>
                                                    </a>
                                                </div>

                                                <a href={"#"}>
                                                    <small className="caption c-blue">@bella</small>
                                                </a>

                                            </div>

                                        </div>
                                    </li>

                                </ul>


                            </div>


                            <div className="col-md-8 order-md-1">
                                <div className="mb-3 subtitle-2">Basic info</div>
                                <form className="needs-validation">
                                    <div className="row">
                                        <div className="col-md-6 mb-3">
                                            <label className={"allcaps"} htmlFor="firstName">First Name</label>
                                            <input type="text" className="form-control"
                                                   name="firstName"
                                                   onChange={this.onChange}
                                                   value={this.state.firstName}
                                            />
                                        </div>
                                        <div className="col-md-6 mb-3">
                                            <label className={"allcaps"} htmlFor="firstName">Last name</label>
                                            <input type="text" className="form-control"
                                                   name="lastName"
                                                   onChange={this.onChange}
                                                   value={this.state.lastName}
                                            />

                                        </div>
                                    </div>

                                    <div className="mb-3">
                                        <label className={"allcaps"} htmlFor="id">Username</label>
                                        <div className="input-group">
                                            <div className="input-group-prepend">
                                                <span className="input-group-text">@</span>
                                            </div>
                                            <input type="text"
                                                   value={this.state.id}
                                                   className="form-control"
                                                   onChange={this.onChange}
                                                   placeholder="Username"
                                                   name="id"
                                            />

                                        </div>
                                    </div>

                                    <div className="mb-3">
                                        <label className={"allcaps"} htmlFor="email">Email</label>
                                        <input type="email" className="form-control" value={this.state.email}
                                               contentEditable={false}
                                               placeholder="you@example.com"/>
                                    </div>

                                    <div className="mb-3">
                                        <label className={"allcaps"} htmlFor="email">Bio</label>
                                        <div contentEditable={true}
                                             name="bio"
                                             className={"card border p-3"}
                                             ref={"bio"}
                                             onChange={this.onChange}>
                                            {this.state.bio}
                                        </div>
                                    </div>


                                    <div className="mb-3 subtitle-2">Work</div>
                                    <div className="row">
                                        <div className="col-md-6 mb-3">
                                            <label className={"allcaps"}>Work as</label>
                                            <input type="text" className="form-control" placeholder=""
                                                   onChange={this.onChange}
                                                   value={this.state.worksAs}
                                                   name="worksAs"
                                            />

                                        </div>
                                        <div className="col-md-6 mb-3">
                                            <label className={"allcaps"} htmlFor="firstName">Company</label>
                                            <input type="text" className="form-control" placeholder=""
                                                   value={this.state.worksAt}
                                                   onChange={this.onChange}
                                                   name="worksAt"
                                            />

                                        </div>
                                    </div>


                                    <div className="mb-3 subtitle-2">Location</div>
                                    <div className="row">
                                        <div className="col-md-6 mb-3">
                                            <label className={"allcaps"} htmlFor="firstName">Country</label>
                                            <input type="text" className="form-control"
                                                   placeholder=""
                                                   onChange={this.onChange}

                                                   value={this.state.country}
                                                   name="country"
                                            />

                                        </div>
                                        <div className="col-md-6 mb-3">
                                            <label className={"allcaps"} htmlFor="firstName">City</label>
                                            <input type="text" className="form-control" id="lastName" placeholder=""
                                                   value={this.state.city}
                                                   onChange={this.onChange}
                                                   name="city"
                                            />
                                        </div>
                                    </div>


                                    <hr className="mb-4"/>
                                    {this.state.loading ? <Loading/> : null}

                                    <button disabled={this.state.firstName && this.state.loading} onClick={this.save}
                                            ref={"saved"}
                                            className="btn btn-primary btn-lg btn-block"
                                            type="submit">
                                        Save
                                    </button>
                                </form>
                            </div>
                        </div>

                    </div>}


            </div>

        )
    }


}

class Shimmer extends Component {
    render() {
        return (<div className="container">
            <div style={{marginRight: "-15px", marginLeft: "-15px",}} className="p-3 rounded-top backg-dark text-left">
                 <span>
                                    <lines className="shine w-25"></lines>
                                    <lines className="shine m-0 w-100"></lines>
                                </span>
            </div>
            <div className="p-3 row">
                <div className="col-md-4 order-md-2 mb-4">
                    <h4 className="d-flex justify-content-between align-items-center mb-3">
                        <lines className="shine w-25"></lines>
                        <span className="shine badge-pill">
                            <lines className="shine w-25"></lines>
                        </span>
                    </h4>
                    <ul className="list-group p-0 mb-5">

                        <li className="d-flex justify-content-between lh-condensed p-0">
                            <div className={"row align-items-center p-0 m-0"}>
                                <div style={{height: "50px", width: "50px"}} className={"mt-2 shine mb-2"}>
                                </div>
                                <div className={"col-sm pl-2"}>
                                    <lines className="shine "></lines>
                                    <lines className="shine "></lines>
                                </div>
                            </div>

                        </li>
                        <li className="d-flex justify-content-between lh-condensed p-0">
                            <div className={"row align-items-center p-0 m-0"}>
                                <div style={{height: "50px", width: "50px"}} className={"mt-2 shine mb-2"}>
                                </div>
                                <div className={"col-sm pl-2"}>
                                    <lines className="shine "></lines>
                                    <lines className="shine "></lines>
                                </div>
                            </div>

                        </li>


                    </ul>


                    <h4 className="d-flex justify-content-between  align-items-center mb-3">
                        <lines className="shine w-25"></lines>
                        <span className="shine badge-pill">
                            <lines className="shine w-25"></lines>
                        </span>
                    </h4>


                    <form className="card border-0">
                        <div className="input-group">
                            <div type="text" className="form-control caption shine mr-1 w-50"/>
                            <div className="input-group-append w-25 shine">
                                <lines className="shine w-25"></lines>
                                <lines className="shine mt-0 w-25"></lines>
                            </div>
                        </div>
                    </form>


                    <ul className="list-group border-top p-0 mb-3">
                        <li className="d-flex justify-content-between lh-condensed p-0">
                            <div className={"row align-items-center p-0 m-0"}>
                                <div style={{height: "50px", width: "50px"}} className={"mt-2 shine mb-2"}>
                                </div>
                                <div className={"col-sm pl-2"}>
                                    <lines className="shine "></lines>
                                    <lines className="shine "></lines>
                                </div>
                            </div>

                        </li>
                        <li className="d-flex justify-content-between lh-condensed p-0">
                            <div className={"row align-items-center p-0 m-0"}>
                                <div style={{height: "50px", width: "50px"}} className={"mt-2 shine mb-2"}>
                                </div>
                                <div className={"col-sm pl-2"}>
                                    <lines className="shine "></lines>
                                    <lines className="shine "></lines>
                                </div>
                            </div>

                        </li>

                    </ul>


                </div>


                <div className="col-md-8 order-md-1">
                    <div className="mb-3 subtitle-2">
                        <lines className="shine "></lines>
                    </div>
                    <form className="needs-validation">
                        <div className="row">
                            <div className="col-md-6 mb-3">
                                <label className={"allcaps shine w-25"} htmlFor="firstName">
                                    <lines className="shine w-25 "></lines>
                                </label>
                                <br/>
                                <lines className="shine w-100"></lines>

                            </div>
                            <div className="col-md-6 mb-3">
                                <label className={"allcaps shine w-25"} htmlFor="firstName">
                                    <lines className="shine w-25 "></lines>
                                </label>
                                <br/>
                                <lines className="shine w-100"></lines>

                            </div>
                        </div>

                        <div className="mb-3">
                            <lines className="shine w-25 "></lines>
                            <div className="input-group">

                                <label className={"allcaps shine w-100"} htmlFor="firstName">
                                    <lines className="shine w-100 "></lines>
                                </label>

                            </div>
                        </div>

                        <div className="mb-3">
                            <lines className="shine w-25 "></lines>
                            <div className="input-group">

                                <label className={"allcaps shine w-100"} htmlFor="firstName">
                                    <lines className="shine w-100 "></lines>
                                </label>

                            </div>
                        </div>

                        <div className="mb-3">
                            <lines className="shine w-25 "></lines>
                            <div className="input-group">

                                <label className={"allcaps shine w-100"} htmlFor="firstName">
                                    <lines className="shine w-100 "></lines>
                                </label>

                            </div>
                        </div>


                        <lines className="shine w-25 "></lines>
                        <div className="row">
                            <div className="col-md-6 mb-3">
                                <label className={"allcaps shine w-25"} htmlFor="firstName">
                                    <lines className="shine w-25 "></lines>
                                </label>
                                <br/>
                                <lines className="shine w-100"></lines>

                            </div>
                            <div className="col-md-6 mb-3">
                                <label className={"allcaps shine w-25"} htmlFor="firstName">
                                    <lines className="shine w-25 "></lines>
                                </label>
                                <br/>
                                <lines className="shine w-100"></lines>

                            </div>
                        </div>


                        <lines className="shine w-25 "></lines>
                        <div className="row">
                            <div className="col-md-6 mb-3">
                                <label className={"allcaps shine w-25"} htmlFor="firstName">
                                    <lines className="shine w-25 "></lines>
                                </label>
                                <br/>
                                <lines className="shine w-100"></lines>

                            </div>
                            <div className="col-md-6 mb-3">
                                <label className={"allcaps shine w-25"} htmlFor="firstName">
                                    <lines className="shine w-25 "></lines>
                                </label>
                                <br/>
                                <lines className="shine w-100"></lines>

                            </div>
                        </div>


                        <hr className="mb-4"/>
                        <div className="shine p-4 btn-lg btn-block">
                        </div>
                    </form>
                </div>
            </div>

        </div>);
    }
}

export default UpdateAbout;