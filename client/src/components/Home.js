// eslint-disable-next-line
import React, {Component} from 'react';
import {Link} from 'react-router-dom';
import '../css/login.css';
import '../css/bootstrap.css';
import '../css/shimmer.css';
import "../css/fa-pro-5.0/fontawesome-pro-5.3.1-web/css/all.css";
import src from '../imgs/favicon.png';
import {getFromStorage} from "../storage/storage";
import HomePost from './HomePost';
import NavBar from "./NavBar";
import Particles from 'react-particles-js';

class Loading extends Component {
    render() {
        return (
            <div>
                <Shimmer/>
            </div>
        );
    }
}

class Shimmer extends Component {
    render() {
        return (
            <div className={"mb-2"}>
                <box style={{maxWidth: 50, height: 50}} className="shine mt-1"></box>

                <div className="shimmer-div">
                    <lines className="shine mt-0" style={{width: 120}}></lines>
                    <lines className="shine"></lines>
                </div>
                <br/>
                <lines className="shine w-100"></lines>
                <lines className="shine w-100"></lines>
                <lines className="shine w-100"></lines>

                <photo className="shine w-100"></photo>
            </div>
        );
    }
}

/*let logout = () => {
/*let logout = () => {
    setInStorage("main-app-storage", null);
};
*/
let userId = "";

function getBase64(file, cb) {
    let reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = function () {
        cb(reader.result)
    };
    reader.onerror = function (error) {
        console.log('Error: ', error);
    };
}

class Home extends Component {
    getuserId = () => {
        return (getFromStorage("main-app-storage").id);
    };

    componentWillMount() {
        if (getFromStorage("main-app-storage")) {
            userId = getFromStorage("main-app-storage").id;
            console.log(userId)
        }
        document.title = 'Trippy | Home';
        // logout();
        if (!getFromStorage("main-app-storage")) {
            this.props.history.push("")
        }
    };

    constructor(props) {
        super(props);
        this.state = {
            loading: false,
            src_64: ""
        }
    }

    fileOnChange = (e) => {
        const file = Array.from(e.target.files);
        let fileBase64;
        getBase64(file[0], (result) => {
            this.state.src_46 = result;
            this.setState({src_64: result});

        })
    };

// eslint-disable
    render() {
        return (
            <div className="h-100">
                <nav className="navbar navbar-expand-lg box-shadow bg-white">
                    <button className="navbar-toggler" type="button" data-toggle="collapse"
                            data-target="#navbarSupportedContent"
                            aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    </button>
                    <div className="collapse container navbar-collapse nav-res" id="navbarSupportedContent">
                        <Link className="navbar-brand m-0" to={"/home"} onClick={() => {
                            this.props.history.push("/home")
                        }} data-toggle="modal" data-target="#exampleModalCenter">

                            <div className="small-rect unselectable backg-yellow subtitle-1 font-weight-bold m-0 c-dark"
                                 style={{fontSize: 12}}>TRIP
                            </div>
                        </Link>
                        <input className="form-control align-self-center form-control-sm c-dark"
                               style={{margin: 'auto', fontSize: 12, maxWidth: 180, fontFamily: 'Rubik,serif'}}
                               type="text"
                               placeholder="Search"/>
                        <Link to={"/profile/" + this.getuserId()} className="mr-3">
                            <i className="fal fa-user-alt c-blue fa-lg art-a">
                            </i>
                        </Link>
                        <a>
                            <i data-container="body"
                               data-toggle="popover"
                               data-placement="bottom"
                               className="fal c-blue fa-globe-americas fa-lg art-a">
                                <span className="artificial-badge">
                                </span>
                            </i>
                        </a>
                        <div id="popover-content" style={{display: "none"}}>
                            <ul className="list-group mt-0 text-left">
                                <li className="row ml-0 cursor_pointer mr-0 mb-0 mt-0 p-0">
                                    <div className="col-sm-1 m-0 p-0">
                                    <span style={{width: "2px", height: "2px", marginTop: "50%"}}
                                          className='backg-pink p-1 rounded-circle position-absolute'>

                                    </span>
                                        <img className="ml-3 mb-0" src={src} height={30} width={30}/>
                                    </div>
                                    <div className="col-sm p-0 m-0  text-left pl-4">
                                        <p className="caption ml-2 p-0 m-0"> Muhammad Saleh Started follwing </p>
                                    </div>
                                </li>

                                <div style={{minHeight: 1, minWidth: "100%", marginTop: "10px", marginBottom: "10px"}}
                                     className={"back-dark-gray"}>

                                </div>
                                <li className="row ml-0 cursor_pointer mr-0 mb-0 mt-0 p-0">
                                    <div className="col-sm-1 m-0 p-0">
                                    <span style={{width: "2px", height: "2px", marginTop: "50%"}}
                                          className='backg-pink p-1 rounded-circle position-absolute'>

                                    </span>
                                        <img className="ml-3 mb-0" src={src} height={30} width={30}/>
                                    </div>
                                    <div className="col-sm p-0 m-0  text-left pl-4">
                                        <p className="caption ml-2 p-0 m-0"> Muhammad Saleh Started follwing </p>
                                    </div>
                                </li>

                                <div style={{minHeight: 1, minWidth: "100%", marginTop: "10px", marginBottom: "10px"}}
                                     className={"back-dark-gray"}>

                                </div>
                                <li className="row ml-0 cursor_pointer mr-0 mb-0 mt-0 p-0">
                                    <div className="col-sm-1 m-0 p-0">
                                    <span style={{width: "2px", height: "2px", marginTop: "50%"}}
                                          className='backg-pink p-1 rounded-circle position-absolute'>

                                    </span>
                                        <img className="ml-3 mb-0" src={src} height={30} width={30}/>
                                    </div>
                                    <div className="col-sm p-0 m-0  text-left pl-4">
                                        <p className="caption ml-2 p-0 m-0"> Muhammad Saleh Started follwing </p>
                                    </div>
                                </li>

                                <div style={{minHeight: 1, minWidth: "100%", marginTop: "10px", marginBottom: "10px"}}
                                     className={"back-dark-gray"}>

                                </div>
                                <li className="row ml-0 cursor_pointer mr-0 mb-0 mt-0 p-0">
                                    <div className="col-sm-1 m-0 p-0">
                                    <span style={{width: "2px", height: "2px", marginTop: "50%"}}
                                          className='backg-pink p-1 rounded-circle position-absolute'>

                                    </span>
                                        <img className="ml-3 mb-0" src={src} height={30} width={30}/>
                                    </div>
                                    <div className="col-sm p-0 m-0  text-left pl-4">
                                        <p className="caption ml-2 p-0 m-0"> Muhammad Saleh Started follwing </p>
                                    </div>
                                </li>

                                <div style={{minHeight: 1, minWidth: "100%", marginTop: "10px", marginBottom: "10px"}}
                                     className={"back-dark-gray"}>

                                </div>
                                <a className="c-blue caption text-center">See more</a>
                            </ul>
                        </div>
                    </div>
                </nav>

                <div className="container mt-3 ">
                    <div className="row m-0">
                        <div className="col-sm-3 col-15 pl-0">

                            <div className=" p-0 col-30 m-0">

                                <Link to={"/local"} style={{fontWeight: "700"}}
                                      className="resize text-left c-dark mt-1">


                                    <div
                                        className="mb-2 unselectable backg-yellow rounded allcaps font-weight-bold p-3 btn-primary c-dark box-shadow"
                                        role="alert">
                                        <i className="far font-weight-bold fa-lg fa-user-friends c-dark mr-2"></i>
                                        Hang out now
                                    </div>

                                </Link>
                                <Link to={"/travel"} style={{fontWeight: "700"}}
                                      className="resize text-left c-dark mt-1">


                                    <div
                                        className="mb-2 unselectable backg-yellow rounded allcaps align-items-center align-content-center font-weight-bold p-0 pt-3 pb-3 pl-3 btn-secondary alert c-dark box-shadow">

                                        <i className="far font-weight-bold fa-lg fa-plane-departure c-dark mr-2"></i>

                                        Travelling Guide

                                    </div>
                                </Link>

                                <div className="alert box-shadow alert-light rounded-left rounded-right bg-white"
                                     role="alert">
                                    <a href="#" style={{fontWeight: "700"}}
                                       className="resize unselectable c-dark caption alert-link">Browse</a>
                                </div>
                                <div className="ml-3">
                                    <div className="alert alert-light bg-white rounded-right yellow-border"
                                         role="alert">
                                        <i style={{marginRight: "19px"}}
                                           className=" c-yellow fas fa-globe-stand fa-lg resize-icon">
                                        </i>
                                        <Link to={"/explore"} style={{fontWeight: "700"}}
                                              className="resize caption  c-dark alert-link">Explore</Link>
                                    </div>

                                    <div className="alert alert-light bg-white rounded-right yellow-border"
                                         style={{marginTop: "-5px"}}
                                         role="alert">
                                        <i style={{marginRight: "19px"}}
                                           className="c-yellow fas fa-route fa-lg resize-icon">
                                        </i>
                                        <Link to={"/explore"} style={{fontWeight: "700"}}
                                              className="resize caption c-dark alert-link">Plans</Link>
                                    </div>


                                    <div className="alert alert-light bg-white yellow-border rounded-right mb-0"
                                         style={{marginTop: "-5px"}} role="alert">
                                        <i style={{marginRight: 12}}
                                           className="c-yellow far fa-user-friends fa-lg resize-icon">
                                        </i>
                                        <Link to={"/trips"} style={{fontWeight: "700"}}
                                              className="resize caption  c-dark alert-link">Trips</Link>
                                    </div>
                                </div>

                                <div
                                    className="alert box-shadow m-0 mt-3 mb-3 alert-light rounded-left rounded-right bg-white"
                                    role="alert">
                                    <a href="#" style={{fontWeight: 700}}
                                       className="resize unselectable caption alert-link c-dark">Services</a>
                                </div>
                                <div className="ml-3">

                                    <div className="alert rounded-right  alert-light mt-1 bg-white dark-border"
                                         style={{marginTop: "-5px"}} role="alert">
                                        <a href="#">
                                            <small href="#" className="resize alert-link caption c-dark">Create place
                                            </small>
                                        </a>
                                    </div>

                                    <div className="alert alert-light rounded-right bg-white dark-border"
                                         style={{marginTop: "-5px"}} role="alert">
                                        <div>
                                            <a href="#">
                                                <small href="#" className="alert-link caption c-dark">Create Agency
                                                </small>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>

                        <div className="col-md-6 col-70" style={{marginBottom: '10px'}}>

                            <SystemNotify/>


                        </div>


                        <div className="col-sm-3 hide-follow pr-0">
                            <div>
                                <div className="alert box-shadow alert-light rounded-left rounded-right bg-white"
                                     role="alert">
                                    <a href="#" style={{fontWeight: 700}}
                                       className="c-dark unselectable caption alert-link">
                                        Who to follow
                                    </a>

                                </div>
                                <div className="list-group">

                                    <Suggestions/>
                                    <Suggestions/>
                                    <Suggestions/>
                                    <Suggestions/>
                                    <Suggestions/>

                                </div>


                                <footer className="bd-footer text-muted mt-2">
                                    <div className="container">
                                        <ul className="list-inline" style={{float: 'left'}}>
                                            <li className="list-inline-item"
                                                style={{fontFamily: 'roboto', fontSize: 10}}>
                                                <a href="#"
                                                   className="text-muted">ABOUT
                                                    US</a></li>
                                            <li className="list-inline-item"
                                                style={{fontFamily: 'roboto', fontSize: 10}}>
                                                <a href="#"
                                                   className="text-muted">SUPPORT</a>
                                            </li>
                                            <li className="list-inline-item"
                                                style={{fontFamily: 'roboto', fontSize: 10}}>
                                                <a href="#"
                                                   className="text-muted">PRESS</a>
                                            </li>
                                            <li className="list-inline-item"
                                                style={{fontFamily: 'roboto', fontSize: 10}}>
                                                <a href="#"
                                                   className="text-muted">API</a>
                                            </li>
                                            <li className="list-inline-item"
                                                style={{fontFamily: 'roboto', fontSize: 10}}>
                                                <a href="#"
                                                   className="text-muted">JOBS</a>
                                            </li>
                                            <li className="list-inline-item"
                                                style={{fontFamily: 'roboto', fontSize: 10}}>
                                                <a href="#"
                                                   className="text-muted">PRIVACY</a>
                                            </li>
                                            <li className="list-inline-item"
                                                style={{fontFamily: 'roboto', fontSize: 10}}>
                                                <a href="#"
                                                   className="text-muted">TERMS</a>
                                            </li>
                                            <li className="list-inline-item"
                                                style={{fontFamily: 'roboto', fontSize: 10}}>
                                                <a href="#"
                                                   className="text-muted">DIRECTORY</a>
                                            </li>
                                            <li className="list-inline-item"
                                                style={{fontFamily: 'roboto', fontSize: 10}}>
                                                <a href="#"
                                                   className="text-muted">PLACES</a>
                                            </li>
                                            <li className="list-inline-item"
                                                style={{fontFamily: 'roboto', fontSize: 10}}>
                                                <a href="#"
                                                   className="text-muted">NEARBY</a>
                                            </li>
                                            <li className="list-inline-item"
                                                style={{fontFamily: 'roboto', fontSize: 10}}>
                                                <a href="#"
                                                   className="text-muted">LANGUAGE</a>
                                            </li>
                                            <li>
                                                <p style={{
                                                    color: '#aeaeae',
                                                    float: 'right',
                                                    fontFamily: 'roboto',
                                                    fontSize: 11
                                                }}>©
                                                    2019
                                                    TRIPPY</p>

                                            </li>
                                        </ul>
                                    </div>
                                </footer>

                            </div>
                        </div>


                    </div>
                </div>

            </div>
        );
    }
}


class Suggestions extends Component {
    render() {
        return (
            <div
                className="list-group-item box-shadow border-0 p-2 flex-column align-items-start mt-2"
            >
                <div className="d-flex w-100 justify-content-between">
                    <div className="form-inline">
                        <img
                            src="https://images.unsplash.com/photo-1528038294373-ae1289d1dbdc?ixlib=rb-0.3.5&s=334597bb997de86ab3655233227e73af&auto=format&fit=crop&w=1050&q=80"
                            style={{width: 40, height: 40}}
                            className="rounded-circle float-left mr-2"
                            alt="..."/>
                        <div>
                            <p className="font-weight-bold"
                               style={{marginBottom: -4, fontSize: "small"}}>
                                <a className="c-blue" href="#">MUSA</a>
                                <span><i
                                    className="fas fa-badge-check fa-xs c-yellow"></i></span>
                            </p>
                            <small className="text-muted">@Don</small>
                        </div>
                    </div>
                    <small><a href="#"><i className="far fa-user-plus c-blue"></i></a></small>
                </div>
            </div>

        );
    }
}


class SystemNotify extends Component {
    render() {
        return (
            <div id="system_notidy">
                <div>
                    <div className="card  backg-dark" style={{marginBottom: '10px'}}>
                        <div className="card-body">
                            <div className="p-0 m-0 row">
                                <div className="mb-3 mr-3">
                                    <img src="https://ucarecdn.com/579ffd9f-1851-40bc-b2f7-3b151299cd38~1/nth/0/"
                                         width="30"/>
                                </div>
                                <div className="col-sm p-0" style={{marginTop: -5}}>
                                    <span className="c-white caption">
                                        How lucky are you!! we just wanted to tell you
                                        that you have been chosen to be part of the festival that will be held in
                                        Belgium 1 Jan 2019 and will last for three days which are completely paid!!! holly yeah let's party
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default Home;