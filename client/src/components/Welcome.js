import React, {Component} from 'react';
import {Link} from "react-router-dom";
import '../css/bootstrap.css';
import '../css/fa-pro-5.0/fontawesome-pro-5.3.1-web/css/all.css';

class Welcome extends Component {

    componentDidMount() {

    }

    render() {
        return (

            <div>
                <section className={"m-0"} style={{padding: 0}}>
                    <nav className="navbar nav-res p-0 m-0 navbar-expand-lg">
                        <a href={"/home"} className={"c-dark rect"}>
                            <div className="font-weight-bold" style={{fontSize: 24}}>TRIP</div>
                        </a>
                        <button className="navbar-toggler" type="button" data-toggle="collapse"
                                data-target="#navbarSupportedContent"
                                aria-controls="navbarSupportedContent" aria-expanded="false"
                                aria-label="Toggle navigation">
                                        <span className="navbar-toggler-icon">
                                            <Link to={'/login'} onClick={() => {
                                                this.props.history.push('/login')
                                            }}>
                                                <i className="c-blue fas fa-user-alt"></i>


                                            </Link>
                                        </span>
                        </button>

                        <div className="collapse navbar-collapse" id="navbarSupportedContent">
                            <ul className="navbar-nav mr-auto">

                            </ul>

                            <Link className="hide-follow" to={'/login'} style={{margin: 30}} onClick={() => {
                                this.props.history.push('/login')
                                window.reload();
                            }}>
                                <i className="c-blue fas fa-user-alt"></i>
                            </Link>


                        </div>


                    </nav>

                    <div className="p-5">
                        <h1 className="font-weight-bold" style={{fontWeight: 700}}>
                            share the story behind each moment in your trip
                        </h1>
                    </div>

                </section>
            </div>
        )
    }
}


export default Welcome