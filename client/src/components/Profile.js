import React, {Component} from 'react';
import {Link, Route, Switch} from 'react-router-dom';
import axios from 'axios';
import '../css/bootstrap.css';
import MasonryLayout from 'react-masonry-component';
import '../css/main.css';
import '../css/shimmer.css';
import PropTypes from 'prop-types';
import src from '../imgs/IMG_20190105_144134070.jpg'
import '../css/animate.min.css';
import Lightbox from 'react-images';
import '../css/shimmer.css';
import "../css/fa-pro-5.0/fontawesome-pro-5.3.1-web/css/all.css";
import {getFromStorage} from "../storage/storage";
import Particles from 'react-particles-js';
import HomePost from './HomePost';
import UpdateAbout from './UpdateAbout';
import ProfileBlogs from "./ProfileBlogs";
import {Tab, Tabs, TabList, TabPanel} from 'react-tabs';
import Img from "./Img";

let pathname = "";
const profileImages = [
    {
        src: 'https://tripppy.herokuapp.com/imgs/favicon.png',
        alt: 'al',
        title: 'San francisco • APRIL • 2018'
    }, {
        src: 'https://images.unsplash.com/photo-1542838687-f960f044ef1b?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=334&q=80',
        alt: 'al',
        title: 'title'
    }, {
        src: 'https://images.unsplash.com/photo-1542838775-551ba96569a8?ixlib=rb-1.2.1&auto=format&fit=crop&w=500&q=60',
        alt: 'al',
        title: 'title'
    }, {
        src: 'https://images.unsplash.com/photo-1545727303-83cede79596a?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=60',
        alt: 'al',
        title: 'title'
    }, {
        src: 'https://images.unsplash.com/photo-1545775866-73f51c143381?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=60',
        alt: 'al',
        title: 'title'
    }, {
        src: 'https://images.unsplash.com/photo-1545829683-1d8935a9b5cd?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=60',
        alt: 'al',
        title: 'title'
    }, {
        src: 'https://images.unsplash.com/photo-1545846340-a45d67f2c377?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=60',
        alt: 'al',
        title: 'title'
    }, {
        src: 'https://images.unsplash.com/photo-1545826145-6d152c800aee?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=60',
        alt: 'al',
        title: 'Baby • FEB  2017'
    }, {
        src: 'https://images.unsplash.com/photo-1501594907352-04cda38ebc29?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=60',
        alt: 'al',
        title: 'Baby • FEB  2017'
    }

];
let posts = [];

class Profile extends Component {

    gotoPrevious() {
        this.setState({
            currentImage: this.state.currentImage - 1,
        });
    }

    gotoNext() {
        this.setState({
            currentImage: this.state.currentImage + 1,
        });
    }

    openLightbox(index, event) {
        event.preventDefault();
        this.setState({
            currentImage: index,
            lightboxIsOpen: true,
        });
    }

    gotoImage(index) {
        this.setState({
            currentImage: index,
        });
    }

    closeLightBox() {
        this.setState({
            currentImage: 0,
            lightboxIsOpen: false,
        });
    }

    getuserId = () => {
        return (getFromStorage("main-app-storage").id);
    };

    getProfileUsername = () => {
        pathname = this.props.history.location.pathname;
        pathname = pathname.replace('/profile/', '');
        pathname = pathname.replace(this.getuserId() + '/', '');
        return pathname;
    }
    setButtons = () => {
        pathname = this.props.history.location.pathname;
        pathname = pathname.replace('/profile/', '');
        pathname = pathname.replace(this.getuserId() + '/', '');
        switch (pathname) {
            case "posts":
                this.setState({page: "p"});
                break;
            case "gallery":
                this.setState({page: "g"});
                break;
            case "blogs":
                this.setState({page: "b"});
                break;
            case "about":
                this.setState({page: "a"});
                break;
            default:
                break;
        }
    };

    constructor(props) {
        super(props);

        this.setButtons();
        this.state = {
            name: "",
            bio: "",
            email: "",
            worksAt: "",
            worksAs: "",
            country: "",
            loading: true,
            page: "p",
            data: {},
            lightboxIsOpen: false,
            currentImage: 0,
            posts: []
        };
        this.closeLightBox = this.closeLightBox.bind(this);
        this.setButtons = this.setButtons.bind(this);
        this.gotoNext = this.gotoNext.bind(this);
        this.gotoPrevious = this.gotoPrevious.bind(this);
        this.gotoImage = this.gotoImage.bind(this);
        this.handleClickImage = this.handleClickImage.bind(this);
        this.openLightbox = this.openLightbox.bind(this);

        pathname = this.props.history.location.pathname;
        pathname = pathname.replace('/profile/', '');
        if (pathname.includes('/')) {

            let counter = pathname.indexOf('/');
            pathname = pathname.substring(0, counter);
        }

        axios.get('/profile?userId=' + pathname)
            .then(response => {
                console.log(response);
                if (response.data) {
                    this.setState({data: response.data.result_user});
                    this.setState({name: response.data.result_user.firstName + " " + response.data.result_user.lastName});
                    this.setState({bio: response.data.result_user.bio});
                    this.setState({worksAt: response.data.result_user.worksAt});
                    this.setState({worksAs: response.data.result_user.worksAs});
                    this.setState({verified: response.data.result_user.verified});
                    this.setState({country: response.data.result_user.country});
                    posts = response.data.result_user.posts;
                    this.setState({email: response.data.result_user.email});
                    this.setState({posts: response.data.result_user.posts.reverse()});
                    this.setState({loading: false});
                    document.title = this.state.name;
                } else {
                    this.props.history.push("/404")
                }

            })
            .catch(error => {
                alert(error.message)
            });
    }

    handleClickImage() {
        if (this.state.currentImage === images.length - 1) return;
        this.gotoNext();
    }

    componentDidUpdate(prevProps) {
        if (this.props.history.location.pathname
            !== prevProps.history.location.pathname) {
            // call the fetch function again
            alert('sho5a5')
        }

    }

    componentWillMount() {
        this.setButtons();

    }


    render() {
        return (
            <div className={"m-0"}>
                <nav className="navbar navbar-expand-lg box-shadow bg-white">
                    <button className="navbar-toggler" type="button" data-toggle="collapse"
                            data-target="#navbarSupportedContent"
                            aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    </button>
                    <div className="collapse container navbar-collapse nav-res" id="navbarSupportedContent">
                        <Link className="navbar-brand m-0" to={"/home"} onClick={() => {
                            this.props.history.push("/home")
                        }} data-toggle="modal" data-target="#exampleModalCenter">

                            <div className="small-rect unselectable backg-yellow subtitle-1 font-weight-bold m-0 c-dark"
                                 style={{fontSize: 12}}>TRIP
                            </div>
                        </Link>
                        <input className="form-control align-self-center form-control-sm c-dark"
                               style={{margin: 'auto', fontSize: 12, maxWidth: 180, fontFamily: 'Rubik,serif'}}
                               type="text"
                               placeholder="Search"/>
                        <Link to={"/profile/" + this.getuserId()} className="mr-3">
                            <i className="fal fa-user-alt c-blue fa-lg art-a">
                            </i>
                        </Link>
                        <a>
                            <i data-container="body"
                               data-toggle="popover"
                               data-placement="bottom"
                               className="fal c-blue fa-globe-americas fa-lg art-a">
                                <span className="artificial-badge">
                                </span>
                            </i>
                        </a>
                        <div id="popover-content" style={{display: "none"}}>
                            <ul className="list-group mt-0 text-left">
                                <li className="row ml-0 cursor_pointer mr-0 mb-0 mt-0 p-0">
                                    <div className="col-sm-1 m-0 p-0">
                                    <span style={{width: "2px", height: "2px", marginTop: "50%"}}
                                          className='backg-pink p-1 rounded-circle position-absolute'>

                                    </span>
                                        <img className="ml-3 mb-0" src={src} height={30} width={30}/>
                                    </div>
                                    <div className="col-sm p-0 m-0  text-left pl-4">
                                        <p className="caption ml-2 p-0 m-0"> Muhammad Saleh Started follwing </p>
                                    </div>
                                </li>

                                <div style={{minHeight: 1, minWidth: "100%", marginTop: "10px", marginBottom: "10px"}}
                                     className={"back-dark-gray"}>

                                </div>
                                <li className="row ml-0 cursor_pointer mr-0 mb-0 mt-0 p-0">
                                    <div className="col-sm-1 m-0 p-0">
                                    <span style={{width: "2px", height: "2px", marginTop: "50%"}}
                                          className='backg-pink p-1 rounded-circle position-absolute'>

                                    </span>
                                        <img className="ml-3 mb-0" src={src} height={30} width={30}/>
                                    </div>
                                    <div className="col-sm p-0 m-0  text-left pl-4">
                                        <p className="caption ml-2 p-0 m-0"> Muhammad Saleh Started follwing </p>
                                    </div>
                                </li>

                                <div style={{minHeight: 1, minWidth: "100%", marginTop: "10px", marginBottom: "10px"}}
                                     className={"back-dark-gray"}>

                                </div>
                                <li className="row ml-0 cursor_pointer mr-0 mb-0 mt-0 p-0">
                                    <div className="col-sm-1 m-0 p-0">
                                    <span style={{width: "2px", height: "2px", marginTop: "50%"}}
                                          className='backg-pink p-1 rounded-circle position-absolute'>

                                    </span>
                                        <img className="ml-3 mb-0" src={src} height={30} width={30}/>
                                    </div>
                                    <div className="col-sm p-0 m-0  text-left pl-4">
                                        <p className="caption ml-2 p-0 m-0"> Muhammad Saleh Started follwing </p>
                                    </div>
                                </li>

                                <div style={{minHeight: 1, minWidth: "100%", marginTop: "10px", marginBottom: "10px"}}
                                     className={"back-dark-gray"}>

                                </div>
                                <li className="row ml-0 cursor_pointer mr-0 mb-0 mt-0 p-0">
                                    <div className="col-sm-1 m-0 p-0">
                                    <span style={{width: "2px", height: "2px", marginTop: "50%"}}
                                          className='backg-pink p-1 rounded-circle position-absolute'>

                                    </span>
                                        <img className="ml-3 mb-0" src={src} height={30} width={30}/>
                                    </div>
                                    <div className="col-sm p-0 m-0  text-left pl-4">
                                        <p className="caption ml-2 p-0 m-0"> Muhammad Saleh Started follwing </p>
                                    </div>
                                </li>

                                <div style={{minHeight: 1, minWidth: "100%", marginTop: "10px", marginBottom: "10px"}}
                                     className={"back-dark-gray"}>

                                </div>
                                <a className="c-blue caption text-center">See more</a>
                            </ul>
                        </div>
                    </div>
                </nav>


                <div className="position-absolute trophy position-fixed">

                    <div
                        className={"allcaps backg-yellow p-1 rounded-right display-none font-weight-bold c-blue unselectable"}>
                        <i className={"fas fa-user-alt  c-blue mr-2"}></i>visit
                    </div>
                    <div
                        className={"allcaps backg-yellow p-1 rounded-right display-none font-weight-bold c-dark unselectable"}>
                        <i className={"fas fa-flag  c-dark mr-1"}></i>guide
                    </div>

                    <div
                        className={"allcaps backg-dark p-1 rounded-right display-none  font-weight-bold c-yellow unselectable "}>
                        <i className={"fas fa-globe-americas  c-yellow mr-1"}></i>enthu
                    </div>
                    <div
                        className={"allcaps trophy backg-dark p-1 rounded-right font-weight-bold c-pink unselectable"}>
                        world
                        <i className={"fas fa-trophy  c-pink ml-1"}></i>
                    </div>
                </div>


                <div className="container p-0 mt-3">


                    <div className="row p-0 m-0">

                        <div className="col-sm p-0">
                            {this.state.loading ? <AboutShimmer/> :
                                <div className="upper-part-center-responsive">
                                    <div className="image">
                                        <img
                                            src={src}
                                            alt={this.state.name}
                                            className="rounded-circle profile-personal-pic  box-shadow"/>

                                        <Lightbox
                                            images={profileImages}
                                            isOpen={this.state.lightboxIsOpen}
                                            onClickPrev={this.gotoPrevious}
                                            currentImage={this.state.currentImage}
                                            onClickNext={this.gotoNext}
                                            onClickImage={this.handleClickImage}
                                            onClose={this.closeLightBox}
                                        />

                                        <span className="img-overlay p-0 rounded-circle">
                                            <div style={{marginTop: "45%"}}>
                                                <a className="p-0 far fa-pencil-alt c-white fa-lg m-1">
                                                </a>
                                                <a onClick={(e) => this.openLightbox(0, e)}
                                                   className="far fa-eye fa-lg c-white m-1">

                                                </a>
                                                <a className="far fa-trash-alt fa-lg c-white m-1">

                                                </a>
                                            </div>
                                        </span>
                                    </div>

                                    <div className="mt-3">
                                        <div className="box-255-w">
                                            <h5 className="font-weight-bold m-0 c-blue">
                                                <a className="c-blue h5">{this.state.name} </a>
                                                <span>
                                                    {this.state.verified ?
                                                        <i className="fas c-yellow fa-badge-check fa-sm"></i> : null}
                                                </span>
                                            </h5>

                                            <div>

                                                <div className="m-0 caption text-muted">
                                                    {
                                                        this.state.worksAs + " "
                                                    }
                                                    at <a className="caption"> {this.state.worksAt}</a>
                                                </div>
                                                <div className="m-0 caption text-muted text-muted">From
                                                    <a className="caption"> {this.state.country}</a>
                                                </div>
                                                <div className="m-0 caption text-muted">Followed by
                                                    <a className="caption"> 15.2 M</a>
                                                </div>

                                            </div>
                                            {console.log(this.getProfileUsername())}
                                            {
                                                getFromStorage("main-app-storage").id === this.state.data.id ? null :
                                                    <button className="btn btn-outline-secondary mt-3">Follow</button>
                                            }

                                            <div className="mt-5">
                                                <div className="box-255-w ">
                                                    <div className="nav-item">
                                                        {this.state.page === "p" ? <Link onClick={() => {
                                                                this.setState({page: "p"})
                                                            }} to={'/profile/' + this.state.data.id + '/posts'}
                                                                                         className="subtitle-1 unselectable m-0 c-yellow">Home</Link> :
                                                            <Link onClick={() => {
                                                                this.setState({page: "p"})
                                                            }} to={'/profile/' + this.state.data.id + '/posts'}
                                                                  className="subtitle-1 unselectable text-muted m-0">Home</Link>}
                                                    </div>
                                                    <div className="nav-item">
                                                        {this.state.page === "g" ?
                                                            <Link onClick={() => {
                                                                this.state.page = "g";
                                                                this.setState({page: "g"})
                                                                this.props.location.push('/profile/' + this.state.data.id + '/gallery')
                                                            }}
                                                                  to={'/profile/' + this.state.data.id + '/gallery'}
                                                                  className="subtitle-1 unselectable m-0 c-yellow">
                                                                Gallery
                                                            </Link> :
                                                            <Link to={'/profile/' + this.state.data.id + '/gallery'}
                                                                  onClick={() => {
                                                                      this.state.page = "g";
                                                                      this.setState({page: "g"})
                                                                  }}
                                                                  className="subtitle-1 unselectable text-muted m-0">Gallery</Link>
                                                        }
                                                    </div>
                                                    <div className="nav-item">
                                                        {this.state.page === "b" ? <Link onClick={() => {
                                                                this.setState({page: "b"})
                                                            }} to={'/profile/' + this.state.data.id + '/blogs'}
                                                                                         className="subtitle-1 m-0 unselectable c-yellow">Blogs</Link> :
                                                            <Link to={'/profile/' + this.state.data.id + '/blogs'}
                                                                  onClick={() => {
                                                                      this.setState({page: "b"})
                                                                  }}
                                                                  className="subtitle-1 text-muted unselectable m-0">Blogs</Link>}
                                                    </div>
                                                    <div className="nav-item">
                                                        {this.state.page === "a" ? <Link onClick={() => {
                                                                this.setState({page: "a"})
                                                            }} to={'/profile/' + this.state.data.id + '/about'}
                                                                                         className="subtitle-1 unselectable m-0 c-yellow">About</Link> :
                                                            <Link onClick={() => {
                                                                this.setState({page: "a"})
                                                            }} to={'/profile/' + this.state.data.id + '/about'}
                                                                  className="subtitle-1 text-muted unselectable m-0">About</Link>}
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            }


                        </div>


                        <Switch>

                            <Route exact path={'/profile/' + this.state.data.id + '/gallery'}
                                   component={() => <Gallery loading={this.state.loading}/>}
                            />

                            <Route exact path={'/profile/' + this.state.data.id + '/blogs'}
                                   component={() => <ProfileBlogs/>}/>
                            <Route exact path={'/profile/' + this.state.data.id + '/about'}
                                   component={() => <UpdateAbout history={this.props.history} data={this.state.data}/>
                                   }
                            />
                        </Switch>


                        <div className="col-md-6 col-70">


                            <div className="profile-a" style={{marginBottom: 10}}>

                                <Switch>
                                    <Route exact path={'/profile/' + this.state.data.id + '/'}
                                           component={() => <PostsSection data={this.state.data}
                                                                          email={this.state.email}
                                                                          history={this.props.history}
                                                                          loading={this.state.loading}/>}/>
                                    <Route exact path={'/profile/' + this.state.data.id + '/posts'}
                                           component={() => <PostsSection data={this.state.data}
                                                                          history={this.props.history}
                                                                          loading={this.state.loading}/>}/>
                                </Switch>

                            </div>
                        </div>

                        <Switch>

                            <Route exact path={'/profile/' + this.state.data.id + '/'}
                                   component={() => <Bio bio={this.state.bio}/>}/>
                            <Route exact path={'/profile/' + this.state.data.id + '/posts'}
                                   component={() => <Bio bio={this.state.bio}/>}/>

                        </Switch>
                    </div>
                </div>
            </div>

        );
    }
}

const masonryOptions = {
    transitionDuration: 0
};
const imagesLoadedOptions = {background: '.backg-dark'};
const images = [
    {
        src: 'https://images.unsplash.com/photo-1545724728-6db195be796e?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=60',
        alt: 'al',
        title: 'San francisco • APRIL • 2018'
    }, {
        src: 'https://images.unsplash.com/photo-1542838687-f960f044ef1b?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=334&q=80',
        alt: 'al',
        title: 'title'
    }, {
        src: 'https://images.unsplash.com/photo-1542838775-551ba96569a8?ixlib=rb-1.2.1&auto=format&fit=crop&w=500&q=60',
        alt: 'al',
        title: 'title'
    }, {
        src: 'https://images.unsplash.com/photo-1545727303-83cede79596a?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=60',
        alt: 'al',
        title: 'title'
    }, {
        src: 'https://images.unsplash.com/photo-1545775866-73f51c143381?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=60',
        alt: 'al',
        title: 'title'
    }, {
        src: 'https://images.unsplash.com/photo-1545829683-1d8935a9b5cd?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=60',
        alt: 'al',
        title: 'title'
    }, {
        src: 'https://images.unsplash.com/photo-1545846340-a45d67f2c377?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=60',
        alt: 'al',
        title: 'title'
    }, {
        src: 'https://images.unsplash.com/photo-1545826145-6d152c800aee?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=60',
        alt: 'al',
        title: 'Baby • FEB  2017'
    }, {
        src: 'https://images.unsplash.com/photo-1501594907352-04cda38ebc29?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=60',
        alt: 'al',
        title: 'Baby • FEB  2018'
    }

];

class Gallery extends Component {
    constructor() {
        super();

        this.state = {
            lightboxIsOpen: false,
            currentImage: 0
        };

        this.closeLightBox = this.closeLightBox.bind(this);
        this.gotoNext = this.gotoNext.bind(this);
        this.gotoPrevious = this.gotoPrevious.bind(this);
        this.gotoImage = this.gotoImage.bind(this);
        this.handleClickImage = this.handleClickImage.bind(this);
        this.openLightbox = this.openLightbox.bind(this);

    }

    gotoPrevious() {
        this.setState({
            currentImage: this.state.currentImage - 1,
        });
    }

    gotoNext() {
        this.setState({
            currentImage: this.state.currentImage + 1,
        });
    }

    openLightbox(index, event) {
        event.preventDefault();
        this.setState({
            currentImage: index,
            lightboxIsOpen: true,
        });
    }

    gotoImage(index) {
        this.setState({
            currentImage: index,
        });
    }

    closeLightBox() {
        this.setState({
            currentImage: 0,
            lightboxIsOpen: false,
        });
    }

    renderGallery() {
        const gallery = images.filter(i => i.src).map((obj, i) => {
            return (
                <div className="col-sm-12 col-md-6 col-lg-4 p-0">

                    <a href={obj.src}
                       key={i}
                       onClick={(e) => this.openLightbox(i, e)}
                       title={obj.title}>
                        <div className="project_box_one">
                            <img
                                src={obj.src}
                                alt={obj.alt}/>
                            <div className="product_info">
                                <div className="product_info_text">
                                    <div className="product_info_text_inner">
                                        <i className="ion ion-plus"></i>
                                        <h4>{obj.title}</h4>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>

            );
        });
        return gallery;

    }

    handleClickImage() {
        if (this.state.currentImage === images.length - 1) return;

        this.gotoNext();
    }

    render() {

        return (
            <div className={"col-lg-9"}>
                <div className="body_block m-0 p-0 pt-4 pb-4 pr-4 pl-4  align-content-center">
                    <div className="container-fluid">
                        <div className="justify-content-center no-gutters">
                            <MasonryLayout
                                className={''}
                                elementType={'div'}
                                options={masonryOptions}
                                updateOnEachImageLoad={true}
                                percentPosition={true}
                                imagesLoadedOptions={imagesLoadedOptions}>

                                {this.renderGallery()}

                            </MasonryLayout>

                            <Lightbox
                                images={images}
                                isOpen={this.state.lightboxIsOpen}
                                onClickPrev={this.gotoPrevious}
                                currentImage={this.state.currentImage}
                                onClickNext={this.gotoNext}
                                onClickImage={this.handleClickImage}
                                onClose={this.closeLightBox}
                            />

                        </div>
                    </div>
                </div>
            </div>

        )
    }
}

Gallery.displayName = 'Gallery';
Gallery.propTypes = {
    heading: PropTypes.string,
    images: PropTypes.array,
    showThumbnails: PropTypes.bool,
    subheading: PropTypes.string,
};

class Bio extends Component {

    render() {
        let params = "";
        if (this.props.bio) {
            params = {
                "particles": {
                    "number": {
                        "value": 50,
                        "density": {
                            "enable": false,
                            "value_area": 900
                        }
                    },
                    "color": {
                        "value": "#ffffff"
                    },
                    "shape": {
                        "type": "circle",
                        "stroke": {
                            "width": 0,
                            "color": "#000000"
                        },
                        "polygon": {
                            "nb_sides": 3
                        },
                        "image": {
                            "src": "",
                            "width": 100,
                            "height": 100
                        }
                    },
                    "opacity": {
                        "value": 0.2,
                        "random": false,
                        "anim": {
                            "enable": false,
                            "speed": 1,
                            "opacity_min": 0.1,
                            "sync": false
                        }
                    },
                    "size": {
                        "value": 2,
                        "random": true,
                        "anim": {
                            "enable": false,
                            "speed": 20,
                            "size_min": 0.1,
                            "sync": false
                        }
                    },
                    "line_linked": {
                        "enable": false,
                        "distance": 150,
                        "color": "#ffffff",
                        "opacity": 0.4,
                        "width": 1
                    },
                    "move": {
                        "enable": true,
                        "speed": 2,
                        "direction": "bottom-right",
                        "random": false,
                        "straight": true,
                        "out_mode": "out",
                        "bounce": false,
                        "attract": {
                            "enable": false,
                            "rotateX": 600,
                            "rotateY": 1200
                        }
                    }
                },
                "interactivity": {
                    "detect_on": "canvas",
                    "events": {
                        "onhover": {
                            "enable": false,
                            "mode": "bubble"
                        },
                        "onclick": {
                            "enable": false,
                            "mode": "repulse"
                        },
                        "resize": true
                    },
                    "modes": {
                        "grab": {
                            "distance": 200,
                            "line_linked": {
                                "opacity": 1
                            }
                        },
                        "bubble": {
                            "distance": 400,
                            "size": 40,
                            "duration": 2,
                            "opacity": 8,
                            "speed": 3
                        },
                        "repulse": {
                            "distance": 200,
                            "duration": 0.4
                        },
                        "push": {
                            "particles_nb": 4
                        },
                        "remove": {
                            "particles_nb": 2
                        }
                    }
                },
                "retina_detect": true
            }
        } else {
            params = {
                "particles": {
                    "number": {
                        "value": 160,
                        "density": {
                            "enable": true,
                            "value_area": 800
                        }
                    },
                    "color": {
                        "value": "#ffffff"
                    },
                    "shape": {
                        "type": "circle",
                        "stroke": {
                            "width": 0,
                            "color": "#000000"
                        },
                        "polygon": {
                            "nb_sides": 5
                        },
                        "image": {
                            "src": "img/github.svg",
                            "width": 100,
                            "height": 100
                        }
                    },
                    "opacity": {
                        "value": 1,
                        "random": true,
                        "anim": {
                            "enable": true,
                            "speed": 1,
                            "opacity_min": 0,
                            "sync": false
                        }
                    },
                    "size": {
                        "value": 3,
                        "random": true,
                        "anim": {
                            "enable": false,
                            "speed": 4,
                            "size_min": 0.3,
                            "sync": false
                        }
                    },
                    "line_linked": {
                        "enable": false,
                        "distance": 150,
                        "color": "#ffffff",
                        "opacity": 0.4,
                        "width": 1
                    },
                    "move": {
                        "enable": true,
                        "speed": 1,
                        "direction": "none",
                        "random": true,
                        "straight": true,
                        "out_mode": "out",
                        "bounce": false,
                        "attract": {
                            "enable": false,
                            "rotateX": 600,
                            "rotateY": 600
                        }
                    }
                },
                "interactivity": {
                    "detect_on": "canvas",
                    "events": {
                        "onhover": {
                            "enable": false,
                            "mode": "repulse"
                        },
                        "onclick": {
                            "enable": false,
                            "mode": "repulse"
                        },
                        "resize": true
                    },
                    "modes": {
                        "grab": {
                            "distance": 400,
                            "line_linked": {
                                "opacity": 1
                            }
                        },
                        "bubble": {
                            "distance": 250,
                            "size": 0,
                            "duration": 2,
                            "opacity": 0,
                            "speed": 3
                        },
                        "repulse": {
                            "distance": 400,
                            "duration": 0.4
                        },
                        "push": {
                            "particles_nb": 4
                        },
                        "remove": {
                            "particles_nb": 2
                        }
                    }
                },
                "retina_detect": true
            }
        }
        return (
            <div className="col-sm-3 p-0 card border-0 backg-dark hide-follow h-100" style={{minHeight: 620}}>

                <div className="w-100 hide-follow h-100">
                    <div className="pr-2 hide-follow pl-2">
                        <p className="subtitle-1 text-white mt-3 ml-2 mb-0  position-absolute unselectable hide-follow">BIOGRAPHY</p>
                        <p className="position-absolute mt-5 ml-2 mr-2 text-white hide-follow caption">
                            {this.props.bio ? this.props.bio : <div className="w-100 unselectable">Loading ...</div>}
                        </p>
                    </div>
                </div>

                <Particles className={"hide-follow"} params={params} style={{minHeight: 620}}/>

            </div>
        )
    }
}


function getBase64(file, cb) {
    let reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = function () {
        cb(reader.result)
    };
    reader.onerror = function (error) {
        console.log('Error: ', error);
    };
}

function Loading() {
    return (
        <div className={"text-center mb-2"}>
            <svg version="1.1" id="loader-1" xmlns="http://www.w3.org/2000/svg"
                 xmlnsXlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                 width="40px" height="40px" viewBox="0 0 40 40" enableBackground="new 0 0 40 40" xmlSpace="preserve">
                <path opacity="0.2" fill="#000" d="M20.201,5.169c-8.254,0-14.946,6.692-14.946,14.946c0,8.255,6.692,14.946,14.946,14.946
    s14.946-6.691,14.946-14.946C35.146,11.861,28.455,5.169,20.201,5.169z M20.201,31.749c-6.425,0-11.634-5.208-11.634-11.634
    c0-6.425,5.209-11.634,11.634-11.634c6.425,0,11.633,5.209,11.633,11.634C31.834,26.541,26.626,31.749,20.201,31.749z"/>
                <path fill="#000" d="M26.013,10.047l1.654-2.866c-2.198-1.272-4.743-2.012-7.466-2.012h0v3.312h0
    C22.32,8.481,24.301,9.057,26.013,10.047z">
                    <animateTransform attributeType="xml"
                                      attributeName="transform"
                                      type="rotate"
                                      from="0 20 20"
                                      to="360 20 20"
                                      dur="0.5s"
                                      repeatCount="indefinite"/>
                </path>
            </svg>
        </div>
    )
}

let months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];

class PostsSection extends Component {
    getProfileUsername = () => {
        pathname = this.props.history.location.pathname;
        pathname = pathname.replace('/profile/', '');
        pathname = pathname.replace(this.getuserId() + '/', '');
        return pathname;
    }
    getuserId = () => {
        return (getFromStorage("main-app-storage").id);
    };

    constructor(props) {
        super(props);
        this.onChange = this.onChange.bind(this);
        this.state = {
            loading: this.props.loading,
            images: [],
            post_content: "",
            email: this.props.email,
            posts: []
        }
    };

    componentWillMount() {
        this.setState({posts: this.props.data.posts})
        this.setState({email: this.props.data.email})
    }

    onChange = (e) => {
        const files = Array.from(e.target.files);
        // works

        this.setState({loading: true});
        getBase64(files[0], (base64) => {
            this.setState({loading: false});
            let tempImage = {
                owner_email: this.state.email,
                dateUploaded: new Date().getDate() + " " + (months[new Date().getMonth()]) + "," + new Date().getFullYear(),
                base64data: "https://images.unsplash.com/photo-1553542792-9c048186c61f?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=634&q=80"
            };
            this.setState({
                images: [...this.state.images, tempImage]
            })
        });
        if (files.length > 3) {
            const msg = 'Only 3 images can be uploaded at a time';
        }
        const types = ['image/png', 'image/jpeg', 'image/gif'];
        files.forEach((file, i) => {

            if (types.every(type => file.type !== type)) {

            }

            if (file.size > 150000) {

            }

        });


        console.log(this.refs.post_content.innerText);
        let temp = this.refs.post_content.innerText;
        this.setState({post_content: temp});

    };


    render() {

        let post = () => {
            this.setState({loading: true});
            this.setState({post_content: this.refs.post_content.innerText});
            this.state.post_content = this.refs.post_content.innerText;
            const h = {};
            h.Accept = 'application/json';
            if (this.state.post_content || this.state.images.length > 0) {
                axios.post('/post', {data: this.state, images: this.state.images}).then(response => {
                    console.log(response.data.post);
                    this.setState({
                        posts: [...this.state.posts, response.data.post]
                    });
                    this.setState({loading: false});
                    this.setState({post_content: ""});
                    this.setState({images: []});
                })
            }
        };


        let onPaste = (e) => {
            e.preventDefault();
            let text = this.refs.post_content.innerText + e.clipboardData.getData('Text');
            this.setState({post_content: text});
        };
        let onKeyUp = (event) => {
            if (event.keyCode === 13) {

            }
        };

        let renderPost = (item, index) => {
            return <HomePost user_email={this.state.email} post_id={item.id}/>
        };

        let renderImg = (item, index) => {

            return (
                <div className="col-sm-12 col-md-6 col-lg-4 p-0">
                    <a href={item.base64data}
                       title="project name 2">
                        <div className="project_box_one">
                            <img
                                src={item.base64data}
                                alt="pro1"/>
                            <div className="product_info">
                                <div className="product_info_text">
                                    <div className="product_info_text_inner justify-content-center">

                                        <button type="button" className="close c-white float-none" style={{opacity: 1}}
                                                data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
            );
        };
        return (
            <div>

                {console.log(this.props.data.id)}

                {
                    getFromStorage("main-app-storage").id === this.props.data.id ?
                        <div className="card p-2 mb-2 app-content">
                            <div className="allcaps mb-2 text-muted">
                                <i className="far fa-marker c-blue pr-1">
                                </i>
                                Write post
                            </div>
                            <div className="p-2 bg-white border caption"
                                 contentEditable={true}
                                 onPaste={onPaste}
                                 onKeyUp={onKeyUp}
                                 ref={"post_content"}
                            >
                                {this.state.post_content}
                            </div>
                            <div className="form-inline">
                                <div className="mt-2">

                                    <Tabs>
                                        <TabList className="nav border-0">
                                            <Tab className="nav-item">
                                                <i className="far fa-image cursor_pointer  c-blue mr-2 mb-2 mt-2 nav-item"></i>
                                            </Tab>
                                            <Tab className="nav-item">
                                                <i className="far fa-video cursor_pointer  c-blue mr-2 mb-2 mt-2"></i>
                                            </Tab>
                                            <Tab className="nav-item">
                                                <i className="far fa-location cursor_pointer c-yellow mr-2 mb-2 mt-2"></i>
                                            </Tab>
                                            <Tab className="nav-item">
                                                <i className="far fa-user-friends cursor_pointer c-yellow mr-2 mb-2 mt-2"></i>
                                            </Tab>
                                            <Tab className="nav-item">
                                                <i className="far fa-calendar-alt cursor_pointer c-dark mr-2 mb-2 mt-2"></i>
                                            </Tab>
                                        </TabList>

                                        <TabPanel className="tab-content border-0 mt-2">
                                            {this.state.loading ?
                                                <div className="position-absolute w-100  p-4 justify-content-center">
                                                    <Loading/>
                                                </div> : null
                                            }

                                            <label htmlFor="file_1">
                                                <div
                                                    className="fal file_input_box text-center box-shadow   fa-plus fa-lg c-white">

                                                </div>
                                            </label>


                                            <input type="file" id="file_1" onChange={this.onChange}
                                                   className="custom_input_file"/>

                                            <div className="row m-0 p-0">
                                                {this.state.images.map(renderImg)}
                                            </div>

                                        </TabPanel>
                                        <TabPanel>
                                            <h2>Any content 2</h2>
                                        </TabPanel>
                                        <TabPanel>
                                            <h2>Any content 2</h2>
                                        </TabPanel>
                                        <TabPanel>
                                            <h2>Any content 2</h2>
                                        </TabPanel>
                                        <TabPanel>
                                            <h2>Any content 2</h2>
                                        </TabPanel>
                                        <TabPanel>
                                            <h2>Any content 2</h2>
                                        </TabPanel>
                                    </Tabs>
                                    <div className='row m-0'>
                                        {this.state.loading ?


                                            <button disabled type="submit"
                                                    onClick={post}
                                                    className="btn btn-outline-secondary backg-blue  allcaps font-weight-bold c-white mr-2 mt-2"
                                                    style={{alignSelf: 'flex-end'}}>Post
                                            </button>
                                            :
                                            < button type="submit"
                                                     onClick={post}
                                                     className="btn btn-outline-secondary backg-blue  allcaps font-weight-bold c-white mr-2 mt-2"
                                                     style={{alignSelf: 'flex-end'}}>Post
                                            </button>

                                        }
                                    </div>

                                </div>

                            </div>

                        </div>
                        :
                        null
                }

                {this.state.posts.length === 0 ? <div
                    className="alert rounded backg-blue alert-dismissible text-center  justify-content-center fade show"
                    role="alert">
                    <button onClick={() => {
                        axios.post('http://www.semsar.city/api/users/login', {
                            params: {
                                email: "m7md.sal762@gmail.com",
                                password: "123"
                            }
                        }).then(result => {
                            console.log(result);
                        })
                    }} type="button" className="btn btn-link float-none backg-blue c-white  font-weight-bold">
                        <span aria-hidden="true">{this.props.data.firstName} has no posts</span>
                    </button>
                </div> : null
                }
                {!this.props.loading ? this.state.posts.map(renderPost) : null}

            </div>

        )
    }
}

function waiting_for_posts() {
    return (<div className="transform-90" title="6">
            <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlnsXlink="http://www.w3.org/1999/xlink"
                 x="0px" y="0px"
                 width="24px" height="30px" viewBox="0 0 24 30">
                <rect x="0" y="0" width="4" height="20" fill="#333">
                    <animate attributeName="opacity" attributeType="XML"
                             values="1; .2; 1"
                             begin="0s" dur="0.6s" repeatCount="indefinite"/>
                </rect>
                <rect x="7" y="0" width="4" height="20" fill="#333">
                    <animate attributeName="opacity" attributeType="XML"
                             values="1; .2; 1"
                             begin="0.2s" dur="0.6s" repeatCount="indefinite"/>
                </rect>
                <rect x="14" y="0" width="4" height="20" fill="#333">
                    <animate attributeName="opacity" attributeType="XML"
                             values="1; .2; 1"
                             begin="0.4s" dur="0.6s" repeatCount="indefinite"/>
                </rect>
            </svg>
        </div>
    );
}

class AboutShimmer
    extends Component {
    render() {
        return (
            <div className={"mb-2"}>

                <div className="upper-part-center-responsive">
                    <div className="rounded-circle shine profile-personal-pic mt-1"/>


                    <div className="mt-3">
                        <div className="box-255-w">
                            <h5 className="font-weight-bold m-0 c-blue">
                    <span>
                    <lines className="shine w-100"></lines>
                    <lines className="shine m-0 w-100"></lines>
                    </span>
                            </h5>

                            <div>

                                <div className="m-0 caption text-muted">

                                    <lines className="shine w-75"></lines>

                                </div>
                                <div className="m-0 caption text-muted text-muted">
                                    <lines className="shine w-75"></lines>

                                </div>
                                <div className="m-0 caption text-muted">
                                    <lines className="shine w-75"></lines>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        );
    }
}

export default Profile;