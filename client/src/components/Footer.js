import React, {Component} from 'react';

class Footer extends Component {
    render() {
        return (<footer className="text-muted m-0 pb-0 text-center m-3 trippy_footer align-items-center">
                <div style={{minHeight: 1, minWidth: "100%", marginTop: "10px", marginBottom: "10px"}}
                     className="back-dark-gray">

                </div>
                <div className="container list-inline pb-0">
                    <ul className="list-inline" style={{float: 'left'}}>
                        <li className="list-inline-item c-white"
                            style={{fontFamily: 'roboto'}}><a
                            className="text-muted allcaps" href="#">ABOUT
                            US</a></li>
                        <li className="list-inline-item c-white"
                            style={{fontFamily: 'roboto'}}><a
                            className={"text-muted allcaps"} href="#">SUPPORT</a>
                        </li>
                        <li className="list-inline-item c-white"
                            style={{fontFamily: 'roboto'}}><a
                            className={"text-muted allcaps"} href="#">PRESS</a>
                        </li>
                        <li className="list-inline-item c-white"
                            style={{fontFamily: 'roboto'}}><a
                            className="text-muted allcaps" href="#">API</a>
                        </li>
                        <li className="list-inline-item c-white"
                            style={{fontFamily: 'roboto'}}><a
                            className="text-muted allcaps" href="#">JOBS</a>
                        </li>
                        <li className="list-inline-item c-white"
                            style={{fontFamily: 'roboto'}}><a
                            className="text-muted allcaps" href="#">PRIVACY</a>
                        </li>
                        <li className="list-inline-item c-white"
                            style={{fontFamily: 'roboto'}}><a
                            className="text-muted allcaps" href="#">TERMS</a>
                        </li>
                        <li className="list-inline-item c-white"
                            style={{fontFamily: 'roboto'}}><a
                            className="text-muted allcaps" href="#">DIRECTORY</a>
                        </li>
                        <li className="list-inline-item c-white"
                            style={{fontFamily: 'roboto'}}><a
                            className={"text-muted allcaps"} href="#">PLACES</a>
                        </li>
                        <li className="list-inline-item c-white"
                            style={{fontFamily: 'roboto'}}><a
                            className={"text-muted allcaps"} href="#">NEARBY</a>
                        </li>
                        <li className="list-inline-item " style={{fontFamily: 'roboto'}}><a
                            className={"text-muted allcaps"} href="#">LANGUAGE</a>
                        </li>
                    </ul>
                    <p className="m-0 text-muted allcaps text-right ">© 2019 TRIPPY</p>
                </div>
            </footer>
        );
    }
}

export default Footer;