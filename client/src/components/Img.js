import React, {Component} from "react";

class Img extends Component {
    render() {
        return (
            <img className="card-img-top"
                 src={this.props.src}
                 alt={this.props.alt}/>
        );
    }
}

export default Img;