import React, {Component} from 'react';
import Img from './Img';
import src from '../imgs/xs.png'
import {Link} from "react-router-dom";
import axios from 'axios';
import {getFromStorage} from "../storage/storage";
import Collapsible from 'react-collapsible';
import '../css/main.css';
import '../css/animate.min.css';

const masonryOptions = {
    transitionDuration: 0
};
const imagesLoadedOptions = {background: '.backg-dark'}

class HomePost extends Component {
    constructor(props) {
        super(props);
        this.state = {
            liked: false,
            comment_body: "",
            pics: [],
            result: {
                post: {likes: [], postPictures: []}
            },
            comments: []
        };
        this.like = this.like.bind(this);
        this.ifLiked = this.ifLiked.bind(this);

    }

    onPaste = (e) => {
        e.preventDefault();
        let text = this.refs.comment_body.innerText + e.clipboardData.getData('Text');
        this.setState({comment_body: text});
    };

    ifLiked() {
        for (let i = 0; i < this.state.result.post.likes.length; i++) {
            if (this.state.result.post.likes[i].liked_by === getFromStorage('main-app-storage').email) {
                this.state.liked = true;
                this.setState({liked: true});
                return;
            }
        }
        this.state.liked = false;
        this.setState({liked: false});
    }

    componentWillMount() {
        axios.get('/getPost', {params: {email: this.props.user_email, id: this.props.post_id}}).then(result => {
            console.log(result.data.result);
            this.setState({result: result.data.result});
            this.setState({comments: result.data.result.post.comments})
            let pic = [];
            for (let i = 0; i < this.state.result.post.postPictures.length; i++) {

                pic.push(this.state.result.post.postPictures[i].base64data);
            }
            this.setState({pics: pic});
            this.ifLiked();
        })
    }

    like() {
        axios.post('/like', {
            liker_email: getFromStorage('main-app-storage').email,
            post_id: this.props.post_id,
            owner_email: this.props.user_email
        }).then(result => {
            console.log(result)

            this.state.liked = result.data.liked;
            this.setState({liked: result.data.liked});
        });
    }

    renderComment = (item, index) => {
        return <Comment comment_id={item.comment_id}
                        user_email={this.props.user_email}
                        post_id={this.props.post_id}

        />
    };

    postComment = () => {
        let text = this.refs.comment_body.innerText;
        this.state.comment_body = text;
        this.setState({comment_body: text});
        console.log(text);
        axios.post('/comment', {
            comment_body: this.state.comment_body,
            po_email: this.props.user_email,
            post_id: this.props.post_id,
            co_email: getFromStorage('main-app-storage').email,
        }).then(result => {
            this.state.comment_body = "";
            this.setState({comment_body: ""});
            console.log(result.data.comment);

            this.setState({comments: [...this.state.comments, result.data.comment]});
        })
    };

    render() {

        return (
            <div>
                <div className="card" style={{marginBottom: '10px'}}>
                    <div className="pb-0 mb-0">

                        <a href="#" style={{color: "black", marginTop: 10}} className="float-right">
                            <i style={{marginRight: 15}} className="far fa-ellipsis-h c-dark"></i>
                        </a>
                        <a href="#">
                            <div style={{marginTop: "-1px"}}
                                 className="mr-5 back-gray font-weight-bold border country-badge border-top-0 float-right p-2 form-inline">
                                <img width="15" src="https://image.flaticon.com/icons/svg/580/580873.svg"/>
                                <p className="allcaps dark-blue m-0 unselectable ml-2">{this.state.result.country}</p>
                            </div>
                        </a>
                        <div className={"row p-0 m-2"}>
                            <img
                                src={this.state.result.avatar}
                                style={{width: 50, height: 50}}
                                className="rounded-circle mt-2 ml-2 mr-2 mb-0 float-left"
                                alt="..."/>
                            <div className={"ml-2"}>
                                <p className="font-weight-bold c-blue" style={{marginTop: 15}}>
                                    <Link to={'/profile/' + this.state.result.username}
                                          onClick={() => {
                                              this.props.history.redirect('/profile/' + this.state.result.username)
                                          }}
                                          className="subtitle-1 c-blue">{this.state.result.name}</Link>
                                    <span>
                                        {this.state.result.verified ?
                                            <i className="fas ml-1 c-yellow fa-badge-check fa-sm"></i> : null}
                                </span>
                                </p>
                                <p className="allcaps"
                                   style={{marginTop: -15}}>
                                    {this.state.result.post.date}
                                </p>
                            </div>
                        </div>
                    </div>
                    <div className="card-body pt-0 mt-0">
                        <p className="c-dark body-2 mt-0 pt-0">{this.state.result.post.body}</p>
                        <div style={{minHeight: 1, minWidth: "100%"}} className={"back-dark-gray mt-2"}></div>
                        <div className="row" style={{marginBottom: -20}}>
                            <div className="col-lg form-inline">
                                {this.state.liked ?
                                    <i className="fas cursor_pointer c-pink fa-heart fa-lg" onClick={this.like}></i> :
                                    <i className="fal cursor_pointer fa-heart fa-lg" onClick={this.like}></i>
                                }
                                <p style={{margin: 19, marginLeft: 10}}
                                   className="rounded-circle float-left" alt="...">
                                    {this.state.result.post.likes.length}</p>
                                <i className="fal fa-comment fa-lg" id={"#comment"}></i>
                                <p style={{margin: 19, marginLeft: 10}}
                                   className="rounded-circle float-right" alt="...">
                                    {this.state.comments.length}</p>

                            </div>
                        </div>

                    </div>
                    <Collapsible contentInnerClassName="pl-2 mb-1" className={"pl-2 mb-1"}
                                 openedClassName={"pl-2 mb-1"}
                                 triggerClassName="c-blue pb-1 allcaps cursor_pointer font-weight-bold"
                                 triggerOpenedClassName="c-blue pb-1 font-weighted-bold allcaps cursor_pointer"
                                 trigger="comments">

                        <div className="mt-2 mb-2">
                            {this.state.comments.map(this.renderComment)}
                        </div>

                        <div className="input-group pr-4">
                            <div className="input-group">
                                <div className="w-100 border p-1  c-dark bg-white  allcaps-placeholder"
                                     contentEditable={true}
                                     style={{borderRadius: 5}}
                                     onPaste={this.onPaste}
                                     onChange={this.onChange}
                                     ref={"comment_body"}
                                >
                                    {this.state.comment_body}
                                </div>
                                <div>
                                    <button className="btn btn-secondary mt-1 allcaps font-weight-bold"
                                            onClick={this.postComment}>
                                        Post
                                    </button>
                                </div>
                            </div>
                        </div>
                    </Collapsible>
                </div>
            </div>
        );
    }

//
}


class Comment extends Component {
    constructor(props) {
        super(props);
        this.state = {
            user: {},
            comment_body: "",
            date: "",
            postId: "",
            comment_owner_email: "",
            comment_id: this.props.comment_id,
            post_owner_email: "",
            likes: [],
            loading: true
        };
        axios.get('/getComment', {
            params: {
                email: this.props.user_email,
                post_id: this.props.post_id,
                comment_id: this.props.comment_id
            }
        }).then(result => {
            console.log(result.data.result);
            this.setState({user: result.data.result.user});
            this.setState({date: result.data.result.date});
            this.setState({likes: result.data.result.likes});
            this.setState({comment_body: result.data.result.comment_body});
            this.setState({comment_owner_email: result.data.result.comment_owner_email});
            this.setState({post_owner_email: result.data.result.post_owner_email});
            this.setState({post_id: result.data.result.post_id});
            this.setState({loading: false})

        })
    }

    render() {
        return (
            <div>
                {
                    this.state.loading ? waiting_for_posts() : <div>
                        <div className="pb-0 mb-0">

                            <a href="#" style={{color: "black", marginTop: 10}} className="float-right">
                                <i style={{marginRight: 15}} className="far fa-ellipsis-h c-dark"></i>
                            </a>

                            <div className={"row m-0"}>

                                <img
                                    src={src}
                                    style={{width: 35, height: 35}}
                                    className="rounded-circle ml-1 mr-1 mb-0 float-left"
                                    alt="..."/>


                                <div className="justify-content-center align-items-center">
                                    <div className="font-weight-bold c-blue justify-content-center align-items-center">
                                        <Link to={'/profile/' + this.state.user.id}
                                              className="caption c-blue">{this.state.user.name}</Link>

                                        {this.state.user.verified ? <span>
                                    <i className="fas ml-1 c-yellow fa-badge-check fa-xs"></i>
                                    </span> : null}

                                        <i className="allcaps m-0">• {this.state.date}</i>
                                    </div>

                                </div>

                            </div>

                            <div className="pl-5 pr-5">

                                {this.state.comment_body}
                            </div>
                        </div>

                        <div>
                            <div className="col-lg m-0 pl-5 form-inline">

                                <i className="fal cursor_pointer c-pink fa-heart fa-sm"></i>
                                <div style={{marginLeft: 10}}
                                     className="rounded-circle float-left">
                                    {this.state.likes.length}
                                </div>
                            </div>
                        </div>
                    </div>
                }

            </div>
        );
    }
}

function waiting_for_posts() {
    return (<div className="transform-90" title="6">
            <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlnsXlink="http://www.w3.org/1999/xlink"
                 x="0px" y="0px"
                 width="24px" height="30px" viewBox="0 0 24 30">
                <rect x="0" y="0" width="4" height="20" fill="#333">
                    <animate attributeName="opacity" attributeType="XML"
                             values="1; .2; 1"
                             begin="0s" dur="0.6s" repeatCount="indefinite"/>
                </rect>
                <rect x="7" y="0" width="4" height="20" fill="#333">
                    <animate attributeName="opacity" attributeType="XML"
                             values="1; .2; 1"
                             begin="0.2s" dur="0.6s" repeatCount="indefinite"/>
                </rect>
                <rect x="14" y="0" width="4" height="20" fill="#333">
                    <animate attributeName="opacity" attributeType="XML"
                             values="1; .2; 1"
                             begin="0.4s" dur="0.6s" repeatCount="indefinite"/>
                </rect>
            </svg>
        </div>
    );
}


export default HomePost;