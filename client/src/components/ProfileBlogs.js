import React, {Component} from 'react';
import Blog from "./Blog";
import ExploreBlog from "./ExploreBlog";
import ad3 from '../imgs/karl-magnuson-396429-unsplash.jpg';
import Place from "./Place";
import {Link} from 'react-router-dom';

class ProfileBlogs extends Component{
    render(){
        return(
            <div className="col-lg-9">
                <Link to={"/edit_blog"} style={{fontWeight: "700"}}
                      className="resize text-left c-yellow mt-1">


                    <div className="mb-2 backg-dark rounded allcaps font-weight-bold p-3 btn-primary c-yellow box-shadow"
                         role="alert">
                        <i className="far fa-lg fa-book c-yellow mr-2"></i>
                       Post blog
                    </div>

                </Link>
                <ExploreBlog
                    desc={"Everyone has at least one idea for the next big thing. What makes startup founders different is their willingness to take action\n" +
                    "to make one of those ideas a realit"}
                    src={ad3}
                    name={"Muhammad Saleh"}
                    date={"26 FEB"}
                    title={"A Traveler’s Guide to Mid-Century"}/>

                <Place
                    desc={"Everyone has at least one idea for the next big thing. What makes startup founders different is their willingness to take action\n" +
                    "to make one of those ideas a realit"}
                    src={ad3}
                    name={"Muhammad Saleh"}
                    date={"26 FEB"}
                    title={"A Traveler’s Guide to Mid-Century"}/>
            </div>
        );
    }
}

export default ProfileBlogs;