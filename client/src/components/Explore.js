import React, {Component} from 'react';
import {Link} from 'react-router-dom';
import '../css/bootstrap.css';
import '../css/main.css';
import '../css/shimmer.css';
import src from '../imgs/karl-magnuson-396429-unsplash.jpg'
import ad3 from '../imgs/karl-magnuson-396429-unsplash.jpg'
import ad4 from '../imgs/tall.jpg'
import ad2 from '../imgs/banner.jpg'

import '../css/shimmer.css';
import "../css/fa-pro-5.0/fontawesome-pro-5.3.1-web/css/all.css";
import {getFromStorage} from "../storage/storage";
import ScrollMenu from 'react-horizontal-scrolling-menu';
import "react-responsive-carousel/lib/styles/carousel.min.css";
import MasonryLayout from 'react-masonry-component';
import HomePost from "./HomePost";
import NavBar from "./NavBar";
import Place from "./Place";
import ExploreBlog from "./ExploreBlog";

const masonryOptions = {
    transitionDuration: 0
};
const imagesLoadedOptions = {background: '.backg-dark'};


const list = [
    {src: src, id: "musa", name: 'Muhammad Saleh'},
    {src: src, id: "bella", name: 'Bella Yassin'},
    {src: src, id: "musa", name: 'Muhammad Saleh'},
    {src: src, id: "bella", name: 'Bella Yassin'},
    {src: src, id: "musa", name: 'Muhammad Saleh'},
    {src: src, id: "bella", name: 'Bella Yassin'},
    {src: src, id: "musa", name: 'Muhammad Saleh'},
    {src: src, id: "bella", name: 'Bella Yassin'},
    {src: src, id: "musa", name: 'Muhammad Saleh'},
    {src: src, id: "bella", name: 'Bella Yassin'},
    {src: src, id: "musa", name: 'Muhammad Saleh'},
    {src: src, id: "bella", name: 'Bella Yassin'},
    {src: src, id: "musa", name: 'Muhammad Saleh'},
    {src: src, id: "bella", name: 'Bella Yassin'},
    {src: src, id: "musa", name: 'Muhammad Saleh'},
    {src: src, id: "bella", name: 'Bella Yassin'},
    {src: src, id: "musa", name: 'Muhammad Saleh'},
    {src: src, id: "bella", name: 'Bella Yassin'},
    {src: src, id: "musa", name: 'Muhammad Saleh'},
    {src: src, id: "bella", name: 'Bella Yassin'},
    {src: src, id: "musa", name: 'Muhammad Saleh'},
    {src: src, id: "bella", name: 'Bella Yassin'},
    {src: src, id: "musa", name: 'Muhammad Saleh'},
    {src: src, id: "bella", name: 'Bella Yassin'},
    {src: src, id: "musa", name: 'Muhammad Saleh'},
    {src: src, id: "bella", name: 'Bella Yassin'},
    {src: src, id: "musa", name: 'Muhammad Saleh'},
    {src: src, id: "bella", name: 'Bella Yassin'},
    {src: src, id: "musa", name: 'Muhammad Saleh'},
    {src: src, id: "bella", name: 'Bella Yassin'}, {src: src, id: "musa", name: 'Muhammad Saleh'},
    {src: src, id: "bella", name: 'Bella Yassin'},
    {src: src, id: "musa", name: 'Muhammad Saleh'},
    {src: src, id: "bella", name: 'Bella Yassin'},
    {src: src, id: "musa", name: 'Muhammad Saleh'},
    {src: src, id: "bella", name: 'Bella Yassin'},
    {src: src, id: "musa", name: 'Muhammad Saleh'},
    {src: src, id: "bella", name: 'Bella Yassin'},
    {src: src, id: "musa", name: 'Muhammad Saleh'},
    {src: src, id: "bella", name: 'Bella Yassin'},

];

const MenuItem = ({id, src, text}) => {
    return (
        <div id={id} className="custom-card m-2 p-2 menu-item" style={{width: 150}}>

            <Link to={"/profile/" + getFromStorage("main-app-storage").id} className={"m-auto"}>
                <img src={src}
                     alt={text}
                     className={"box-shadow  rounded-circle m-auto"}
                     width={100} height={100}/></Link>

            <div className={"m-auto text-center"}>
                <div className={"subtitle-2 mt-2"}>{text} <i className="fas c-yellow fa-badge-check fa-sm">

                </i>
                </div>
                <Link to={"/profile/" + getFromStorage("main-app-storage").id}
                      className={"c-blue m-auto text-center allcaps"}>@{id}</Link><br/>
            </div>
            <div className="mt-2 text-center ">

                <button className="btn-primary  backg-yellow w-50 c-dark btn allcaps border-0 p-2 m-auto">Follow
                </button>
            </div>
        </div>
    );
};

export const Menu = (list) => list.map(el => {
    const {name} = el;
    const {id} = el;
    const {src} = el;

    return (
        <MenuItem src={src} id={id} text={name} key={name}/>
    );
});
const ArrowLeftCo = () => {
    return (
        <i className={"fal m-2 fa-arrow-circle-left fs-sm c-blue"}>

        </i>
    );
};
const ArrowRightCo = () => {
    return (
        <i className={"fal m-2 fa-arrow-circle-right fs-sm c-blue"}>

        </i>
    );
};

const ArrowLeft = ArrowLeftCo({text: '<', className: 'arrow-prev'});
const ArrowRight = ArrowRightCo({text: '>', className: 'arrow-next'});


class Explore extends Component {

    getuserId = () => {
        return (getFromStorage("main-app-storage").id);
    };

    componentDidMount() {
        document.title = "Explore";
    }


    onSelect = key => {
        this.setState({selected: key});
    };

    state = {
        selected: 0
    };

    render() {
        const {selected} = this.state;

        // Create menu from items
        const menu = Menu(list, selected);
        return (
            <div>

                <nav className="navbar navbar-expand-lg box-shadow bg-white">
                    <button className="navbar-toggler" type="button" data-toggle="collapse"
                            data-target="#navbarSupportedContent"
                            aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    </button>
                    <div className="collapse container navbar-collapse nav-res" id="navbarSupportedContent">
                        <Link className="navbar-brand m-0" to={"/home"} onClick={() => {
                            this.props.history.push("/home")
                        }} data-toggle="modal" data-target="#exampleModalCenter">

                            <div className="small-rect unselectable backg-yellow subtitle-1 font-weight-bold m-0 c-dark"
                                 style={{fontSize: 12}}>TRIP
                            </div>
                        </Link>
                        <input className="form-control align-self-center form-control-sm c-dark"
                               style={{margin: 'auto', fontSize: 12, maxWidth: 180, fontFamily: 'Rubik,serif'}}
                               type="text"
                               placeholder="Search"/>
                        <Link to={"/profile/" + this.getuserId()} className="mr-3">
                            <i className="fal fa-user-alt c-blue fa-lg art-a">
                            </i>
                        </Link>
                        <a>
                            <i data-container="body"
                               data-toggle="popover"
                               data-placement="bottom"
                               className="fal c-blue fa-globe-americas fa-lg art-a">
                                <span className="artificial-badge">
                                </span>
                            </i>
                        </a>
                        <div id="popover-content" style={{display: "none"}}>
                            <ul className="list-group mt-0 text-left">
                                <li className="row ml-0 cursor_pointer mr-0 mb-0 mt-0 p-0">
                                    <div className="col-sm-1 m-0 p-0">
                                    <span style={{width: "2px", height: "2px", marginTop: "50%"}}
                                          className='backg-pink p-1 rounded-circle position-absolute'>

                                    </span>
                                        <img className="ml-3 mb-0" src={src} height={30} width={30}/>
                                    </div>
                                    <div className="col-sm p-0 m-0  text-left pl-4">
                                        <p className="caption ml-2 p-0 m-0"> Muhammad Saleh Started follwing </p>
                                    </div>
                                </li>

                                <div style={{minHeight: 1, minWidth: "100%", marginTop: "10px", marginBottom: "10px"}}
                                     className={"back-dark-gray"}>

                                </div>
                                <li className="row ml-0 cursor_pointer mr-0 mb-0 mt-0 p-0">
                                    <div className="col-sm-1 m-0 p-0">
                                    <span style={{width: "2px", height: "2px", marginTop: "50%"}}
                                          className='backg-pink p-1 rounded-circle position-absolute'>

                                    </span>
                                        <img className="ml-3 mb-0" src={src} height={30} width={30}/>
                                    </div>
                                    <div className="col-sm p-0 m-0  text-left pl-4">
                                        <p className="caption ml-2 p-0 m-0"> Muhammad Saleh Started follwing </p>
                                    </div>
                                </li>

                                <div style={{minHeight: 1, minWidth: "100%", marginTop: "10px", marginBottom: "10px"}}
                                     className={"back-dark-gray"}>

                                </div>
                                <li className="row ml-0 cursor_pointer mr-0 mb-0 mt-0 p-0">
                                    <div className="col-sm-1 m-0 p-0">
                                    <span style={{width: "2px", height: "2px", marginTop: "50%"}}
                                          className='backg-pink p-1 rounded-circle position-absolute'>

                                    </span>
                                        <img className="ml-3 mb-0" src={src} height={30} width={30}/>
                                    </div>
                                    <div className="col-sm p-0 m-0  text-left pl-4">
                                        <p className="caption ml-2 p-0 m-0"> Muhammad Saleh Started follwing </p>
                                    </div>
                                </li>

                                <div style={{minHeight: 1, minWidth: "100%", marginTop: "10px", marginBottom: "10px"}}
                                     className={"back-dark-gray"}>

                                </div>
                                <li className="row ml-0 cursor_pointer mr-0 mb-0 mt-0 p-0">
                                    <div className="col-sm-1 m-0 p-0">
                                    <span style={{width: "2px", height: "2px", marginTop: "50%"}}
                                          className='backg-pink p-1 rounded-circle position-absolute'>

                                    </span>
                                        <img className="ml-3 mb-0" src={src} height={30} width={30}/>
                                    </div>
                                    <div className="col-sm p-0 m-0  text-left pl-4">
                                        <p className="caption ml-2 p-0 m-0"> Muhammad Saleh Started follwing </p>
                                    </div>
                                </li>

                                <div style={{minHeight: 1, minWidth: "100%", marginTop: "10px", marginBottom: "10px"}}
                                     className={"back-dark-gray"}>

                                </div>
                                <a className="c-blue caption text-center">See more</a>
                            </ul>
                        </div>
                    </div>
                </nav>


                <div className={"bg-white "}>

                    <ScrollMenu
                        data={menu}
                        arrowLeft={ArrowLeft}
                        arrowRight={ArrowRight}
                        wheel={false}
                        dragging={false}
                        menuStyle={{backgroundColor: "#fff"}}
                        selected={selected}
                        forwardClick={true}
                        alignCenter={true}
                        onSelect={this.onSelect}
                    />
                </div>


                <div className={"container"}>
                    <div className="row  m-0 mt-2 p-0">
                        <div className="col-sm m-0"></div>
                        <div className={"col-md-6 col-70 m-0 p-1"}>
                            <MasonryLayout
                                className={''} // default ''
                                elementType={'div'} // default 'div'
                                options={masonryOptions} // default {}// default false
                                updateOnEachImageLoad={true}
                                percentPosition={true}// default false and works only if disableImagesLoaded is false
                                imagesLoadedOptions={imagesLoadedOptions}>

                                <div>
                                    <ExploreBlog
                                        desc={"Everyone has at least one idea for the next big thing. What makes startup founders different is their willingness to take action\n" +
                                        "to make one of those ideas a realit"}
                                        src={ad3}
                                        date={"26 FEB"}
                                        title={"A Traveler’s Guide to Mid-Century"}

                                        name={"Muhammad Saleh"}/>
                                    <ExploreBlog
                                        desc={"A Traveler’s Guide to Mid-Century Modern Architecture in New York City A Traveler’s Guide to Mid-Century Modern Architecture in New York City\n"}
                                        title={"A Traveler’s Guide to Mid-Century"}
                                        src={ad2}
                                        date={"sponsored"}
                                        name={"Muhammad Saleh"}/>
                                    <div className="p-1">


                                    </div>
                                    <Place
                                        desc={"Everyone has at least one idea for the next big thing. What makes startup founders different is their willingness to take action\n" +
                                        "to make one of those ideas a realit"}
                                        src={ad4}
                                        name={"Grand Cafe"}
                                        date={"sponsored"}
                                        title={"A Traveler’s Guide to Mid-Century"}/>
                                    <Place
                                        desc={"Everyone has at least one idea for the next big thing. What makes startup founders different is their willingness to take action\n" +
                                        "to make one of those ideas a realit"}
                                        src={ad4}
                                        name={"Grand Cafe"}
                                        date={"sponsored"}
                                        title={"A Traveler’s Guide to Mid-Century"}/>

                                    <HomePost
                                        content={"Hello, World! Welcome to Trippy. Trippy is a traveler experience community for travel bolgs and travelling enthusiastics. http://localhost:3000/profile/musa/about" +
                                        "Trippy is the place where you can share your trip to show anyone the adventure and experience you have faced Hope you enjoy it and don't forget to send me your feedback!"}
                                        date={"Sep 6 • 12:28 AM"} name={"Muhammad Saleh"} postHasImg={true}
                                        src={src}/>

                                    <HomePost date={"Sep 6 • 12:28 AM"} name={"Muhammad Saleh"} postHasImg={true}
                                              src={src}/>

                                    <HomePost date={"Sep 6 • 12:28 AM"} name={"Muhammad Saleh"} postHasImg={true}
                                              src={src}/>


                                </div>
                            </MasonryLayout>

                        </div>
                        <div className="col-sm m-0"></div>
                    </div>
                </div>
            </div>


        )
    }

}

export default Explore;