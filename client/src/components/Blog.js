import React, {Component} from 'react';
import '../css/bootstrap.css';
import "../css/fa-pro-5.0/fontawesome-pro-5.3.1-web/css/all.css";
import {Link, Route, Switch, Router} from 'react-router-dom';

class Blog extends Component {
    render() {
        return (
            <div className={"mb-2"}>
                <div className="m-0 p-0">
                    <div className="card pr-0">

                        <div className={"row m-4"}>
                            <img
                                src="https://images.pexels.com/photos/629142/pexels-photo-629142.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940"
                                style={{width: 50, height: 50}}
                                className="rounded-circle m-2 float-left"
                                alt="..."/>
                            <div className={"ml-2"}>
                                <p className="font-weight-bold c-blue" style={{marginTop: 15}}>
                                    <Link to="/home" className="subtitle-1 c-blue">Muhammad Saleh</Link>
                                    <span>
                                    <i className="fas ml-1 c-yellow fa-badge-check fa-sm"></i>
                                </span>
                                </p>
                                <p className="allcaps"
                                   style={{fontSize: 10, marginTop: -15}}>
                                    26 Feb
                                </p>
                            </div>
                        </div>

                        <a href="#" title="A Traveler’s Guide to Mid-Century Modern Architecture in New York City" className="card border-0 pl-3 pr-3">
                            <img className="card-img-top"
                                 src="https://images.pexels.com/photos/96450/pexels-photo-96450.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940"
                                 alt="A Traveler’s Guide to Mid-Century Modern Architecture in New York City"/>
                        </a>

                        <p className="c-dark pl-4  pr-4 pt-4 font-weight-bold font-rubik reduce-font font-24">
                            A Traveler’s Guide to Mid-Century Modern Architecture in New York City
                        </p>
                        <div className="pl-4 pb-2 form-inline">
                            <i className="fa fa-thumbs-up fa-lg c-blue"></i>
                            <p style={{margin: 19, marginLeft: 10}}
                               className="rounded-circle float-left">
                                1.4K
                            </p>
                            <a title="Save" className="pr-4 btn right-0 position-absolute">
                                <i className="far fa-bookmark m-2 fa-lg c-dark"></i>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default Blog;