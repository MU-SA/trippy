import React,{Component} from "react";
import {Link} from 'react-router-dom';

class Trip extends Component {
    render() {
        return (

            <div className="m-0 mb-2  p-0">
                <div className="card pr-0">
                    <div className="position-absolute right-0" style={{marginTop:"-1px"}}>
                        <i title={"Trip"} className="far fa-suitcase   p-2 backg-yellow c-dark"></i>
                        <i title={"Ad"} className="far  fa-ad  p-2  c-dark"></i>

                    </div>

                    <div className="row  m-2">

                        <img
                            src="https://images.pexels.com/photos/629142/pexels-photo-629142.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940"
                            style={{width: 50, height: 50}}
                            className="rounded-circle m-1 float-left"
                            alt="..."/>
                        <div className={"ml-1"}>
                            <p className="font-weight-bold c-blue" style={{marginTop: 10}}>
                                <Link to="/home" className="subtitle-1 c-dark">{this.props.name}</Link>
                                <span>
                                    <i className="fas ml-1 c-yellow fa-badge-check fa-sm">

                                    </i>
                                </span>
                            </p>
                            <p className="allcaps" style={{fontSize: 10, marginTop: -15}}>
                                {this.props.date}
                            </p>
                        </div>
                    </div>

                    <a href="#" title={this.props.name}
                       className="card border-0 ">
                        <img className="card-img-top rounded-0"
                             src={this.props.src}
                             alt="A Traveler’s Guide to Mid-Century Modern Architecture in New York City"/>
                    </a>

                    <table className="table-responsive m-1">
                        <tr>
                            <td><i className="far p-1 fa-plane-departure text-muted"></i></td>
                            <td>Egypt</td>
                            <td><i className="far p-1 fa-plane-arrival text-muted"></i></td>
                            <td>Luxur</td>
                            <td><i className="far p-1 fa-car text-muted"></i></td>
                            <td>Bus</td>
                        </tr>
                        <tr>
                            <td><i className="far p-1 fa-money-bill text-muted"></i></td>
                            <td>500</td>
                            <td><i className="far p-1 fa-calendar text-muted"></i></td>
                            <td>1 Weak</td>
                            <td><i className="far p-1 fa-sign text-muted"></i></td>
                            <td>no pets</td>
                        </tr>
                    </table>
                    <div className="pl-2 pb-2 form-inline btn-group">
                        <button className="btn btn-primary">enroll</button>
                        <button className="btn btn-outline-primary">more details</button>
                    </div>
                </div>
            </div>

        );
    }
}
export default Trip;