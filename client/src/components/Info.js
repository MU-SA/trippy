import React, {Component} from 'react';

class Info extends Component {
    render() {
        return (
            <div className="text-center align-content-center justify-content-center float-center">
                <div>
                    <p style={{marginTop: 10, color: '#005cbf'}}
                       className="font-weight-bold text-center">{this.props.name}</p>
                    <p className="small" style={{marginTop: -10, color: '#969696'}}>{this.props.occupy}</p>
                    <div style={{marginTop: -10}}
                         className="row text-center d-flex align-items-center justify-content-center small">
                        <i className="material-icons">location_on</i>
                        <p style={{marginBottom: -2}} className="small">{this.props.location}</p>
                    </div>
                </div>
                <div className="row" style={{marginTop: 10}}>
                    <div className="col-4">
                        <p className="font-weight-bold">{this.props.followersCount}</p>
                        <p className="text-muted">Followers</p>
                    </div>

                    <div className="col-4">
                        <p className="font-weight-bold">{this.props.followingCount}</p>
                        <p className="text-muted">Following</p>
                    </div>

                    <div className="col-4">
                        <p className="font-weight-bold">{this.props.postsCount}</p>
                        <p className="text-muted">Posts</p>
                    </div>
                </div>
                <div style={{backgroundColor: '#dadae4', height: 1, marginBottom: 8}}>

                </div>

                <div className="container text-center">
                    <p className="small">{this.props.desc}</p>
                </div>
            </div>

        );
    }
}

export default Info;