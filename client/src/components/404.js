import React, {Component} from 'react';
import '../css/bootstrap.css';
import '../css/404.css';
import {Link} from 'react-router-dom';

class Pg404 extends Component {

    render() {
        return (
            <div>
                <nav className="navbar navbar-expand-lg fixed-top">
                    <button className="navbar-toggler" type="button" data-toggle="collapse"
                            data-target="#navbarSupportedContent"
                            aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">

                    </button>

                    <div className="collapse container navbar-collapse nav-res" id="navbarSupportedContent">

                        <Link className="navbar-brand m-0" to={"/home"} onClick={() => {
                            this.props.history.push("/home")
                        }} data-toggle="modal" data-target="#exampleModalCenter">

                            <div className="small-rect backg-yellow font-weight-bold m-0 c-dark"
                                 style={{fontSize: 12}}>TRIP
                            </div>

                        </Link>

                        <input
                            className="form-control bg-white border-0 align-self-center form-control-sm c-dark"
                            style={{margin: 'auto', fontSize: 12, maxWidth: 180, fontFamily: 'Rubik,serif'}}
                            type="text"
                            placeholder="Search"/>

                        <a href="#" className="mr-3 invisible">
                            <i className="fal fa-user-alt c-blue fa-lg art-a">
                            </i>
                        </a>
                    </div>

                </nav>

                <div className="background-404 c-white">
                    <div className="child c-white">

                        <div style={{lineHeight: 5}}>
                            <div className=" font-weight-bold">
                                <h6 className="d-404 c-yellow m-0 p-0 text-center">404</h6>
                                <h4 className="text-center">Not Found</h4>
                            </div>

                            <div className="child h-auto form-inline m-auto">
                                <Link to={"/home"} onClick={()=>{
                                    this.props.history.push("/home")
                                }}><p className="text-center allcaps">Return Home</p></Link>
                                <i className="fas fa-circle font-small-8 mb-3 ml-1 mr-1"></i>
                                <Link to={"/"} onClick={()=>{}}><p className="text-center allcaps">Go Back</p></Link>
                            </div>
                        </div>
                    </div>
                </div>


            </div>
        )
    }
}

export default Pg404