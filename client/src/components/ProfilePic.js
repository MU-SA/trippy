import React, {Component} from 'react';

class ProfilePic extends Component {
    render() {
        return (
            <div>
                <img className="rounded-circle" style={{height: 150, width: 150, marginTop: -75}}
                     src="../imgs/xs.png"/>
            </div>
        );
    }
}

export default ProfilePic;