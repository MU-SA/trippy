import {getFromStorage} from "../storage/storage";
import React, {Component} from 'react';
import ScrollMenu from 'react-horizontal-scrolling-menu';
import src from '../imgs/karl-magnuson-396429-unsplash.jpg'

import {Link} from 'react-router-dom';
const ArrowLeftCo = () => {
    return (
        <i className={"fal m-2 fa-arrow-circle-left fs-sm c-blue"}>

        </i>
    );
};
const ArrowRightCo = () => {
    return (
        <i className={"fal m-2 fa-arrow-circle-right fs-sm c-blue"}>

        </i>
    );
};

const MenuItem = ({id, src, text}) => {
    return (
        <div id={id} className="custom-card m-2 p-2 menu-item" style={{width: 150}}>

            <Link to={"/profile/" + getFromStorage("main-app-storage").id} className={"m-auto"}>
                <img src={src}
                     alt={text}
                     className={"box-shadow  rounded-circle m-auto"}
                     width={100} height={100}/></Link>

            <div className={"m-auto text-center"}>
                <div className={"subtitle-2 mt-2"}>{text} <i className="fas c-yellow fa-badge-check fa-sm">

                </i>
                </div>
                <Link to={"/profile/" + getFromStorage("main-app-storage").id}
                      className={"c-blue m-auto text-center allcaps"}>@{id}</Link><br/>
            </div>
            <div className="mt-2 text-center ">

                <button className="btn-primary  backg-yellow w-50 c-dark btn allcaps border-0 p-2 m-auto">Follow
                </button>
            </div>
        </div>
    );
};
const list = [
    {src: src, id: "musa", name: 'Muhammad Saleh'},
    {src: src, id: "bella", name: 'Bella Yassin'},
    {src: src, id: "musa", name: 'Muhammad Saleh'},
    {src: src, id: "bella", name: 'Bella Yassin'},
    {src: src, id: "musa", name: 'Muhammad Saleh'},
    {src: src, id: "bella", name: 'Bella Yassin'},
    {src: src, id: "musa", name: 'Muhammad Saleh'},
    {src: src, id: "bella", name: 'Bella Yassin'},
    {src: src, id: "musa", name: 'Muhammad Saleh'},
    {src: src, id: "bella", name: 'Bella Yassin'},
    {src: src, id: "musa", name: 'Muhammad Saleh'},
    {src: src, id: "bella", name: 'Bella Yassin'},
    {src: src, id: "musa", name: 'Muhammad Saleh'},
    {src: src, id: "bella", name: 'Bella Yassin'},
    {src: src, id: "musa", name: 'Muhammad Saleh'},
    {src: src, id: "bella", name: 'Bella Yassin'},
    {src: src, id: "musa", name: 'Muhammad Saleh'},
    {src: src, id: "bella", name: 'Bella Yassin'},
    {src: src, id: "musa", name: 'Muhammad Saleh'},
    {src: src, id: "bella", name: 'Bella Yassin'},
    {src: src, id: "musa", name: 'Muhammad Saleh'},
    {src: src, id: "bella", name: 'Bella Yassin'},
    {src: src, id: "musa", name: 'Muhammad Saleh'},
    {src: src, id: "bella", name: 'Bella Yassin'},
    {src: src, id: "musa", name: 'Muhammad Saleh'},
    {src: src, id: "bella", name: 'Bella Yassin'},
    {src: src, id: "musa", name: 'Muhammad Saleh'},
    {src: src, id: "bella", name: 'Bella Yassin'},
    {src: src, id: "musa", name: 'Muhammad Saleh'},
    {src: src, id: "bella", name: 'Bella Yassin'}, {src: src, id: "musa", name: 'Muhammad Saleh'},
    {src: src, id: "bella", name: 'Bella Yassin'},
    {src: src, id: "musa", name: 'Muhammad Saleh'},
    {src: src, id: "bella", name: 'Bella Yassin'},
    {src: src, id: "musa", name: 'Muhammad Saleh'},
    {src: src, id: "bella", name: 'Bella Yassin'},
    {src: src, id: "musa", name: 'Muhammad Saleh'},
    {src: src, id: "bella", name: 'Bella Yassin'},
    {src: src, id: "musa", name: 'Muhammad Saleh'},
    {src: src, id: "bella", name: 'Bella Yassin'},

];


const ArrowLeft = ArrowLeftCo({text: '<', className: 'arrow-prev'});
const ArrowRight = ArrowRightCo({text: '>', className: 'arrow-next'});
export const Menu = (list) => list.map(el => {
    const {name} = el;
    const {id} = el;
    const {src} = el;

    return (
        <MenuItem src={src} id={id} text={name} key={name}/>
    );
});

class ScrollItems extends Component {
    onSelect = key => {
        this.setState({selected: key});
    };

    state = {
        selected: 0
    };

    render() {
        const {selected} = this.state;

        // Create menu from items
        const menu = Menu(list, selected);
        return (<ScrollMenu
            data={menu}
            arrowLeft={ArrowLeft}
            arrowRight={ArrowRight}
            wheel={false}
            dragging={false}
            menuStyle={{backgroundColor: "#fff"}}
            selected={selected}
            forwardClick={true}
            alignCenter={true}
            onSelect={this.onSelect}
        />);
    }
}

export  default ScrollItems;