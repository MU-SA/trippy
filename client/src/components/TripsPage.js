import React, {Component} from "react";
import {Link} from 'react-router-dom';
import '../css/bootstrap.css';
import '../css/main.css';
import src from '../imgs/karl-magnuson-396429-unsplash.jpg'
import {getFromStorage} from "../storage/storage";
import Trip from "./Trip";
import ad3 from '../imgs/karl-magnuson-396429-unsplash.jpg'
import ad4 from '../imgs/tall.jpg'
import ad2 from '../imgs/banner.jpg'

class TripsPage extends Component {

    getuserId = () => {
        return (getFromStorage("main-app-storage").id);
    };

    render() {
        return (
            <div>
                <nav className="navbar navbar-expand-lg box-shadow bg-white">
                    <button className="navbar-toggler" type="button" data-toggle="collapse"
                            data-target="#navbarSupportedContent"
                            aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    </button>
                    <div className="collapse container navbar-collapse nav-res" id="navbarSupportedContent">
                        <Link className="navbar-brand m-0" to={"/home"} onClick={() => {
                            this.props.history.push("/home")
                        }} data-toggle="modal" data-target="#exampleModalCenter">

                            <div className="small-rect unselectable backg-yellow subtitle-1 font-weight-bold m-0 c-dark"
                                 style={{fontSize: 12}}>TRIP
                            </div>
                        </Link>
                        <input className="form-control align-self-center form-control-sm c-dark"
                               style={{margin: 'auto', fontSize: 12, maxWidth: 180, fontFamily: 'Rubik,serif'}}
                               type="text"
                               placeholder="Search"/>
                        <Link to={"/profile/" + this.getuserId()} className="mr-3">
                            <i className="fal fa-user-alt c-blue fa-lg art-a">
                            </i>
                        </Link>
                        <a>
                            <i data-container="body"
                               data-toggle="popover"
                               data-placement="bottom"
                               className="fal c-blue fa-globe-americas fa-lg art-a">
                                <span className="artificial-badge">
                                </span>
                            </i>
                        </a>
                        <div id="popover-content" style={{display: "none"}}>
                            <ul className="list-group mt-0 text-left">
                                <li className="row ml-0 cursor_pointer mr-0 mb-0 mt-0 p-0">
                                    <div className="col-sm-1 m-0 p-0">
                                    <span style={{width: "2px", height: "2px", marginTop: "50%"}}
                                          className='backg-pink p-1 rounded-circle position-absolute'>

                                    </span>
                                        <img className="ml-3 mb-0" src={src} height={30} width={30}/>
                                    </div>
                                    <div className="col-sm p-0 m-0  text-left pl-4">
                                        <p className="caption ml-2 p-0 m-0"> Muhammad Saleh Started follwing </p>
                                    </div>
                                </li>

                                <div style={{minHeight: 1, minWidth: "100%", marginTop: "10px", marginBottom: "10px"}}
                                     className={"back-dark-gray"}>

                                </div>
                                <li className="row ml-0 cursor_pointer mr-0 mb-0 mt-0 p-0">
                                    <div className="col-sm-1 m-0 p-0">
                                    <span style={{width: "2px", height: "2px", marginTop: "50%"}}
                                          className='backg-pink p-1 rounded-circle position-absolute'>

                                    </span>
                                        <img className="ml-3 mb-0" src={src} height={30} width={30}/>
                                    </div>
                                    <div className="col-sm p-0 m-0  text-left pl-4">
                                        <p className="caption ml-2 p-0 m-0"> Muhammad Saleh Started follwing </p>
                                    </div>
                                </li>

                                <div style={{minHeight: 1, minWidth: "100%", marginTop: "10px", marginBottom: "10px"}}
                                     className={"back-dark-gray"}>

                                </div>
                                <li className="row ml-0 cursor_pointer mr-0 mb-0 mt-0 p-0">
                                    <div className="col-sm-1 m-0 p-0">
                                    <span style={{width: "2px", height: "2px", marginTop: "50%"}}
                                          className='backg-pink p-1 rounded-circle position-absolute'>

                                    </span>
                                        <img className="ml-3 mb-0" src={src} height={30} width={30}/>
                                    </div>
                                    <div className="col-sm p-0 m-0  text-left pl-4">
                                        <p className="caption ml-2 p-0 m-0"> Muhammad Saleh Started follwing </p>
                                    </div>
                                </li>

                                <div style={{minHeight: 1, minWidth: "100%", marginTop: "10px", marginBottom: "10px"}}
                                     className={"back-dark-gray"}>

                                </div>
                                <li className="row ml-0 cursor_pointer mr-0 mb-0 mt-0 p-0">
                                    <div className="col-sm-1 m-0 p-0">
                                    <span style={{width: "2px", height: "2px", marginTop: "50%"}}
                                          className='backg-pink p-1 rounded-circle position-absolute'>

                                    </span>
                                        <img className="ml-3 mb-0" src={src} height={30} width={30}/>
                                    </div>
                                    <div className="col-sm p-0 m-0  text-left pl-4">
                                        <p className="caption ml-2 p-0 m-0"> Muhammad Saleh Started follwing </p>
                                    </div>
                                </li>

                                <div style={{minHeight: 1, minWidth: "100%", marginTop: "10px", marginBottom: "10px"}}
                                     className={"back-dark-gray"}>

                                </div>
                                <a className="c-blue caption text-center">See more</a>
                            </ul>
                        </div>
                    </div>
                </nav>

                <div className={"bg-white mt-2"}>
                    <div className="card border-0 container">
                        <div className="col-md-6 m-auto">
                            <div className="allcaps form-inline text-muted mt-3 mb-3 ">Search Options
                                <span
                                    className="allcaps ml-2 font-weight-bold color-yellow cursor_pointer  border-0">reset</span>
                            </div>

                            <div className="input-group">
                                <div className="col-sm  mt-1  p-rv" style={{paddingLeft:"0px",paddingRight:"0.5rem"}}>
                                    <select className="form-control  c-dark border-primary   allcaps  container">

                                        <option className="custom-select c-dark allcaps p-2">Category</option>
                                        <option className="custom-select">Default</option>

                                    </select>
                                </div>
                                <div className="col-sm mt-1 p-rv" style={{paddingLeft:"0px",paddingRight:"0.5rem"}}>
                                    <select className="form-control  c-dark border-primary   allcaps  container">

                                        <option className="custom-select">Moods</option>
                                        <option className="custom-select">Default</option>

                                    </select>
                                </div>
                                <div className="col-sm mt-1 p-rv" style={{paddingLeft:"0px",paddingRight:"0.5rem"}}>
                                    <select className="form-control  c-dark border-primary   allcaps  container">
                                        <option className="custom-select">Location</option>
                                        <option className="custom-select">Default</option>
                                    </select>
                                </div>
                                <div className="col-sm p-0 mt-1">
                                    <select className="form-control  c-dark border-primary   allcaps  container">
                                        <option className="custom-select">Search area</option>
                                        <option className="custom-select">Default</option>

                                    </select>
                                </div>

                            </div>
                        </div>
                        <div className="input-group col-md-6 m-auto">
                            <div className="input-group">

                                <button type="submit"
                                        className="btn btn-lg btn-outline-primary w-100 backg-yellow  allcaps font-weight-bold c-dark">Search
                                </button>
                            </div>

                        </div>

                    </div>

                </div>


                <div className={""}>
                    <div className="row p-0  m-0 mt-2">


                        <div className="col-sm m-0">

                            <Trip
                                desc={"Everyone has at least one idea for the next big thing. What makes startup founders different is their willingness to take action\n" +
                                "to make one of those ideas a realit"}
                                src={ad4}
                                name={"Grand Cafe"}
                                date={"sponsored"}
                                ad={"yes"}
                                title={"A Traveler’s Guide to Mid-Century"}/>

                            <Trip
                                desc={"Everyone has at least one idea for the next big thing. What makes startup founders different is their willingness to take action\n" +
                                "to make one of those ideas a realit"}
                                src={ad4}
                                name={"Grand Cafe"}
                                date={"sponsored"}
                                title={"A Traveler’s Guide to Mid-Century"}/>

                            <Trip
                                desc={"Everyone has at least one idea for the next big thing. What makes startup founders different is their willingness to take action\n" +
                                "to make one of those ideas a realit"}
                                src={ad4}
                                name={"Grand Cafe"}
                                date={"sponsored"}
                                title={"A Traveler’s Guide to Mid-Century"}/>


                        </div>

                        <div className="col-sm m-0 p-0 col-70" style={{marginBottom: '10px'}}>

                            <Trip
                                desc={"Everyone has at least one idea for the next big thing. What makes startup founders different is their willingness to take action\n" +
                                "to make one of those ideas a realit"}
                                src={ad3}
                                name={"Muhammad Saleh"}
                                date={"26 FEB"}
                                title={"A Traveler’s Guide to Mid-Century"}/>
                            <Trip
                                content={"Hello, World! Welcome to Trippy. Trippy is a traveler experience community for travel bolgs and travelling enthusiastics. http://localhost:3000/profile/musa/about" +
                                "Trippy is the place where you can share your trip to show anyone the adventure and experience you have faced Hope you enjoy it and don't forget to send me your feedback!"}
                                date={"Sep 6 • 12:28 AM"} name={"Muhammad Saleh"} postHasImg={true}
                                src={src}/>
                            <Trip
                                content={"Hello, World! Welcome to Trippy. Trippy is a traveler experience community for travel bolgs and travelling enthusiastics. http://localhost:3000/profile/musa/about" +
                                "Trippy is the place where you can share your trip to show anyone the adventure and experience you have faced Hope you enjoy it and don't forget to send me your feedback!"}
                                date={"Sep 6 • 12:28 AM"} name={"Muhammad Saleh"} postHasImg={true}
                                src={src}/>
                            <Trip
                                content={"Hello, World! Welcome to Trippy. Trippy is a traveler experience community for travel bolgs and travelling enthusiastics. http://localhost:3000/profile/musa/about" +
                                "Trippy is the place where you can share your trip to show anyone the adventure and experience you have faced Hope you enjoy it and don't forget to send me your feedback!"}
                                date={"Sep 6 • 12:28 AM"} name={"Muhammad Saleh"} postHasImg={true}
                                src={src}/>


                        </div>


                        <div className="col-sm m-0">
                            <Trip
                                desc={"Everyone has at least one idea for the next big thing. What makes startup founders different is their willingness to take action\n" +
                                "to make one of those ideas a realit"}
                                src={ad3}
                                name={"Gazef"}
                                date={"26 feb • 2018"}
                                title={"A Traveler’s Guide to Mid-Century"}/>

                            <Trip
                                desc={"Everyone has at least one idea for the next big thing. What makes startup founders different is their willingness to take action\n" +
                                "to make one of those ideas a realit"}
                                src={ad4}
                                name={"Grand Cafe"}
                                date={"sponsored"}
                                title={"A Traveler’s Guide to Mid-Century"}/>

                            <Trip
                                desc={"Everyone has at least one idea for the next big thing. What makes startup founders different is their willingness to take action\n" +
                                "to make one of those ideas a realit"}
                                src={ad4}
                                name={"Grand Cafe"}
                                date={"sponsored"}
                                title={"A Traveler’s Guide to Mid-Century"}/>


                        </div>

                    </div>

                </div>

            </div>
        );
    }
}

export default TripsPage;