import React, {Component} from "react";
import {Link} from 'react-router-dom';

class ExploreBlog extends Component {
    constructor(props) {
        super(props)
    }

    render() {
        return (
            <div className="m-0 mb-2  p-0">
                <div className="card pr-0">
                    <div className={"row m-2"}>
                        <a href="#" style={{color: "black", marginTop: 10}} className="position-absolute right-0">
                            <i style={{marginRight: 15}} className="far fa-bookmark c-dark">

                            </i>
                            <i style={{marginRight: 15}} className="far fa-ellipsis-h c-dark">

                            </i>
                        </a>
                        <img
                            src="https://images.pexels.com/photos/629142/pexels-photo-629142.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940"
                            style={{width: 50, height: 50}}
                            className="rounded-circle m-1 float-left"
                            alt="..."/>
                        <div className={"ml-1"}>
                            <p className="font-weight-bold c-blue" style={{marginTop: 10}}>
                                <Link to="/home" className="subtitle-1 c-blue">{this.props.name}</Link>
                                <span>
                                    <i className="fas ml-1 c-yellow fa-badge-check fa-sm">

                                    </i>
                                </span>
                            </p>
                            <p className="allcaps"
                               style={{fontSize: 10, marginTop: -15}}>
                                {this.props.date}
                            </p>
                        </div>
                    </div>

                    <a href="#" title={this.props.title}
                       className="card border-0 ">
                        <img className="card-img-top rounded-0"
                             src={this.props.src}
                             alt="A Traveler’s Guide to Mid-Century Modern Architecture in New York City"/>
                    </a>

                    <div className="c-dark pl-2 form-inline  pr-2 pt-4 reduce-font body-1">
                        <div className="subtitle-1">{this.props.title}</div>
                        <span>
                            <div className="font-weight-bold ml-2 country-badge  p-2 form-inline">
                                <img width="15" src="https://image.flaticon.com/icons/svg/580/580873.svg"/>
                                <p className="allcaps m-0 text-muted unselectable ml-2">EGYPT</p>
                            </div>
                        </span>
                    </div>
                    <p className=" pl-2  pr-2  text-muted font-rubik reduce-font body-1">
                        {this.props.desc}
                    </p>
                    <div className="pl-2 pb-2 form-inline">
                        <i className="far fa-thumbs-up fa-lg c-blue">

                        </i>
                        <a href="#" style={{margin: 19, marginLeft: 10}}
                           className="c-blue rounded-circle  float-left">
                            1.4K
                        </a>
                        <i className="far fa-comment fa-lg c-blue">
                        </i>
                        <p style={{margin: 19, marginLeft: 10}}
                           className="rounded-circle c-blue float-left">
                            850
                        </p>
                    </div>
                </div>
            </div>

        );
    }

}

export default ExploreBlog;