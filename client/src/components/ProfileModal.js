import React, {Component} from 'react';
import {Link} from 'react-router-dom';
import axios from 'axios';
import '../css/bootstrap.css';
import '../register/css/style.css';
import {getFromStorage, setInStorage} from '../storage/storage';
let message = "";

function Errors() {
    return (

        <div className={"p-2 alert border-0 card backg-dark right-0"}>
            <div className="row c-white" role="alert">
                <div className={"col-sm-1"} style={{marginTop: "3px"}}>
                    <i className="c-pink fal fa-exclamation-circle">

                    </i>
                </div>

                <div className={"align-items-start"}>
                    <p className={"c-white p-0 m-0 subtitle-l"}>{message.header.trim()}</p>
                    <p className={"text-muted p-0 mb-0 caption"}>{message.message}</p>
                </div>
            </div>


        </div>

    );
}

function Loading() {
    return (
        <div className={"text-center mb-2"}>
            <svg version="1.1" id="loader-1" xmlns="http://www.w3.org/2000/svg"
                 xmlnsXlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                 width="40px" height="40px" viewBox="0 0 40 40" enableBackground="new 0 0 40 40" xmlSpace="preserve">
                <path opacity="0.2" fill="#000" d="M20.201,5.169c-8.254,0-14.946,6.692-14.946,14.946c0,8.255,6.692,14.946,14.946,14.946
    s14.946-6.691,14.946-14.946C35.146,11.861,28.455,5.169,20.201,5.169z M20.201,31.749c-6.425,0-11.634-5.208-11.634-11.634
    c0-6.425,5.209-11.634,11.634-11.634c6.425,0,11.633,5.209,11.633,11.634C31.834,26.541,26.626,31.749,20.201,31.749z"/>
                <path fill="#000" d="M26.013,10.047l1.654-2.866c-2.198-1.272-4.743-2.012-7.466-2.012h0v3.312h0
    C22.32,8.481,24.301,9.057,26.013,10.047z">
                    <animateTransform attributeType="xml"
                                      attributeName="transform"
                                      type="rotate"
                                      from="0 20 20"
                                      to="360 20 20"
                                      dur="0.5s"
                                      repeatCount="indefinite"/>
                </path>
            </svg>
        </div>
    )
}


class ModalProfile extends Component {

    componentWillMount() {
        document.title = 'Trippy | Login';
        if(getFromStorage("main-app-storage")){
            this.props.history.push("");
        }
    };

    constructor(props) {
        super(props);
        message = {
            header: "Email is not valid",
            message: "Enter a valid email address"
        };
        this.state = {
            validate: true,
            exist: false,
        };
        this.onChange = this.onChange.bind(this);
        this.login = this.login.bind(this);
    }

    onChange = (e) => {
        this.setState({validate: true});
        this.setState({[e.target.name]: [e.target.value]});
    };


    login = (e) => {
        this.setState({loading: true});
        e.preventDefault();
        // Optionally the request above could also be done as
        axios.get('/login', {params: {email: this.state.email, password: this.state.password}})
            .then(response => {
                this.setState({loading: false});
                if (response.data.valid === false) {
                    message = {
                        header: "Email is not valid",
                        message: "Enter a valid email address"
                    };
                    this.setState({validate: false})
                } else if (response.data.valid === true) {
                    if (response.data.exist === true) {
                        this.setState({validate: true});
                        console.log();
                        setInStorage("main-app-storage", response.data.user);
                        this.props.history.push("/home");
                    } else if (response.data.exist === false) {
                        message = {
                            header: "Error",
                            message: "User name or password doesn't match"
                        };
                        this.setState({validate: false})
                    }
                }

            })
            .catch(function (error) {
                console.log(error);
            });
    };

    render() {
        return (
            <section>
                <Link to={''} className={"c-dark"}>
                    <div className="rect fixed-top font-weight-bold nav-res" style={{fontSize: 24}}>TRIP</div>
                </Link>

                <div className="register-container">
                    <div className="row mt-3 mb-3">
                        <div className="input-group">
                            <input type="email" name={"email"} onChange={this.onChange}
                                   placeholder="Email Address"/>
                        </div>
                        <div className="input-group">
                            <input type="password" name={"password"} onChange={this.onChange}
                                   placeholder="Password"/>
                        </div>
                    </div>

                    {this.state.validate ? null : <Errors/>}

                    {this.state.loading ? <Loading/> : null}

                    <button className="r-btn mb-3 button-text c-white backg-blue border-0"
                            onClick={this.login}
                            disabled={this.state.loading}
                            style={{height: 60, width: 150, fontSize: 'small'}}>Log in
                    </button>
                    <div className="m-auto text-center  caption">Don't have account?
                        <Link to={'/register'} onClick={() => {
                            this.props.history.push('/register')
                        }}> Create one</Link>
                    </div>
                </div>
            </section>

        )
    }
}

export default ModalProfile