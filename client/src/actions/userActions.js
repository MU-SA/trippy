import axios from 'axios';
import {ADD_USER, GET_USER, DELETE_USER, USERS_LOADING} from '../types'
function setItemsLoading() {
    return {
        type: USERS_LOADING
    };
}

export const register = user => dispatch => {
    dispatch(setItemsLoading());
    console.log(user);
    axios.post('/register', user).then(res =>
        dispatch({
            type: ADD_USER,
            payload: res.data
        })
    );
};
