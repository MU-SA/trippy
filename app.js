const createError = require('http-errors');
const express = require('express');
const path = require('path');
const mongoose = require('mongoose');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const db = require('./config/keys').mongoURI;
const morgan = require('morgan');
const exphbs = require('express-handlebars');
const flash = require('connect-flash');
const session = require('express-session');
const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;
const expressValidator = require('express-validator');
const indexRouter = require('./routes/index');
var debug = require('debug')('expressjs:server');
var http = require('http');
const usersRouter = require('./routes/register');
const login = require('./routes/login');
const profile = require('./routes/profile');
const post = require('./routes/post');
const getPost = require('./routes/getPost');
const getComment = require('./routes/getComment');
const like = require('./routes/like');
const comment = require('./routes/comment');
const formData = require('express-form-data');
const app = express();

// Express Validator
app.use(expressValidator({
    errorFormatter: function (param, msg, value) {
        var namespace = param.split('.')
            , root = namespace.shift()
            , formParam = root;

        while (namespace.length) {
            formParam += '[' + namespace.shift() + ']';
        }
        return {
            param: formParam,
            msg: msg,
            value: value
        };
    }
}));

// view engine setup

app.use(morgan('dev'));
app.use(express.json({limit:'50mb'}));

app.use(bodyParser.json({limit: '50mb'}));
app.use(bodyParser.urlencoded({limit: '50mb', extended: true}));
app.use(cookieParser());
// app.use(formData.parse());
app.use(express.static(path.join(__dirname, 'public')));
app.use('/register', usersRouter);
app.use('/login', login);
app.use('/profile', profile);
app.use('/post', post);
app.use('/getPost', getPost);
app.use('/like', like);
app.use('/getComment', getComment);
app.use('/comment', comment);

mongoose.connect(db, {useNewUrlParser: true}).then(() => {
    console.log('connected')
}).catch(err => {
    console.log(err.message);

});

// error handler

// Serve static assets if in production
if (process.env.NODE_ENV === 'production') {
    // Set static folder
    app.use(express.static('client/build'));
    app.get('/*', (req, res) => {
        let url = path.join(__dirname, 'client/build', 'index.html');
        if (!url.startsWith('/app/')) // we're on local windows
            url = url.substring(1);
        res.sendFile(url);
    });
    // app.get('*', (req, res) => {
    //     res.sendFile(path.resolve(__dirname, 'client', 'build', 'index.html'));
    // });
}
const port = process.env.PORT || 5000;
app.listen(port, () => console.log(`Server started on port ${port}`));
module.exports = app;
