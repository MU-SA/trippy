/*
* photo types
* 0 -> profile pic
* 1 -> post photo
* 2 -> blog photo
* 3 -> comment photo
* */
const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const photo = new Schema({
    owner_email: {
        type: String
    },
    dateUploaded: {
        type: String
    },
    base64data:{
        type:String
    }

});
//
const likes = new Schema({
    liked_by: {
        type: String
    }, date: {
        type: String
    }
});
const postComment = new Schema({
    comment_body: {type: String},
    date: {type: String},
    postId: {type: String},
    comment_owner_email: {type: String},
    comment_id: {type: String},
    post_owner_email: {type: String},
    likes: [likes],
    user: {}

});

const post = new Schema({
    body: {
        type: String
    },
    postPictures: [photo],
    likes: [likes],
    date: String,
    comments: [postComment],
    id: String
});

const User = new Schema({
    id: {type: String},
    firstName: {
        type: String,
        required: true
    },
    lastName: {
        type: String,
        required: true
    }, middleName: {
        type: String,
        required: true
    }, email: {
        type: String,
        required: true,
        unique: true
    },
    password: {
        type: String,
        required: true
    },
    gender: {
        type: String,
        required: true
    },
    followers: [],
    following: [],
    country: {
        type: String,
        required: true
    },
    day_of_birth: {
        type: String,
        required: true
    },
    month_of_birth: {
        type: String,
        required: true
    },
    year_of_birth: {
        type: String,
        required: true
    },
    posts: [post],
    worksAt: {
        type: String,
    },
    worksAs: {
        type: String,
    },
    bio: {
        type: String,
        default: ""
    },
    verified: {
        type: Boolean,
        default: false
    }
    , gallery: {
        type: Array
    },
    city: {
        type: String
    }

});

module.exports = user = mongoose.model('user', User);