const mongoose = require('mongoose');

const Date = new mongoose.Schema({
    Year: {type: String},
    Month: String,
    Day: String,
    Hour: String,
    Minute: String
});
const Photo = new mongoose.Schema({
    URL: String,
    CreatedAt: Date,
    Type: String,
    Description: String
});
const Location = new mongoose.Schema({
    Country: String,
    City: String,
    GMLongitude: String,
    GMLatitude: String
});
const Like = new mongoose.Schema({
    CreatedAt: Date,
    CreatedById: String,
    ContentId: String,
    ContentOwnerId: String
});
const Search = new mongoose.Schema({
    SearchedAt: Date,
    SearchedFrom: Location,
    KeyWord: String,

});
const Follow = new mongoose.Schema({
    FollowerId: String,
    FollowingId: String,
    FollowedAt: Date,

});
const Comment = new mongoose.Schema({
    CreatedAt: Date,
    CreatedById: String,
    ContentId: String,
    ContentOwnerId: String,
    Title: String,
    Likes: [Like]

});
const Blog = new mongoose.Schema({
    Title: String,
    OwnerId: String,
    Images: [Photo],
    BlogId: String,
    CreatedAt: Date,
    Description: String,
    Location: Location,
    Views: String,
    Likes: [Like],
    Comments: [Comment]
});
const Post = new mongoose.Schema({
    OwnerId: String,
    PostId: String,
    CreatedAt: Date,
    Country: Location,
    Likes: [Like],
    Comments: [Comment],
    Images: [Photo]

});
const Place = new mongoose.Schema({
    id: String,
    AdministratorId: String,
    CreatedAt: Date,
    Location: Location
});
const User = new mongoose.Schema({
    FirstName: String,
    LastName: String,
    Email: String,
    UserId: String,
    LiveIn: Location,
    Password: String,
    Posts: [Post],
    Gallery: [Photo],
    Blogs: [Blog],
    Search: [Search],
    Avatar: String,
    Gender: String,
    Birthday: String,
    Followers: [Follow],
    Following: [Follow],
    Verified: Boolean,
    Bio: String,
    WorksAs: String,
    Places: [Place],
    WorksAt: Place,
});

module.exports = mongoose.model('User', User);